package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/commandff/gdleparse/ac"
)

// ac.Dat Public Interfaces:
var Portal ac.DatFile = ac.DatFile{Filename: "client_portal.dat"}
var Cell ac.DatFile = ac.DatFile{Filename: "client_cell_1.dat"}
var HighRes ac.DatFile = ac.DatFile{Filename: "client_highres.dat"}
var LocalEnglish ac.DatFile = ac.DatFile{Filename: "client_local_English.dat"}

//

var CacheBin = &ac.CPhatDataBin{
	FileName:   "",
	FileSize:   0,
	Version:    0,
	NumEntries: 0,
	Entries: []ac.CPhatDataBinEntry{
		{FMagic1: 0xe8b00434, FMagic2: 0x82092270, SegName: "_regionData"},
		{FMagic1: 0x5D97BAEC, FMagic2: 0x41675123, SegName: "_spellTableData"},
		{FMagic1: 0x7DC126EB, FMagic2: 0x5F41B9AD, SegName: "_treasureTableData"},
		{FMagic1: 0x5F41B9AD, FMagic2: 0x7DC126EB, SegName: "_craftTableData"},
		{FMagic1: 0x887aef9c, FMagic2: 0xa92ec9ac, SegName: "_housePortalDests"},
		{FMagic1: 0xcd57fd07, FMagic2: 0x697a2224, SegName: "_data inferred cell data"},
		{FMagic1: 0x591c34e9, FMagic2: 0x2250b020, SegName: "avatarTable"},
		{FMagic1: 0xE80D81CA, FMagic2: 0x8ECA9786, SegName: "_questDefDB"},
		{FMagic1: 0xd8fd6b02, FMagic2: 0xa0427974, SegName: "CWeenieDefaults"},
		{FMagic1: 0x5f1fa913, FMagic2: 0xe345c74c, SegName: "mutation_table_t"},
		{FMagic1: 0x812a7823, FMagic2: 0x8b28e107, SegName: "_gameEvents"},
		{FMagic1: 0, FMagic2: 0, SegName: "Error"},
	},
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	fmt.Printf("boop\n")
	content, err := os.ReadFile("gdle.weenies.id.1342179251.data.bin")
	if err == nil {
		poop := new(ac.CWeenieSave).Read(content)
		log.Printf("CWeenieSave(1342179251): WeenieType: %d, WCID: %d, StringStats: %s\n", poop.Qualities.WeenieType, poop.Qualities.WCID, ac.ToJSON(poop.Qualities.StringStats))
		log.Printf("QuestTable: %s\n", ac.ToJSON(poop.QuestTable))
		os.WriteFile("1342179251.json", []byte(ac.ToJSONFormat(poop, "", "  ")), 0600)
	}
	// Cell.Parse("C:\\Projects\\")
	// Cell.Stats()
	// ent, contentany := Cell.GetDIDObj(0xBBA0FFFF)
	// content := contentany.(*ac.Dat_LandBlock)
	// log.Printf("DumpDID(0xBBA0FFFF): %s\n\t%s", ent.ToString(&Cell), ac.ToJSONFormat(content, "", "  "))
	// CacheBin.Load("cache.bin")
	// Portal.Parse("C:\\Projects\\")
	// Portal.Stats()
	// for _, did := range Portal.PortalCat[0x06] {
	// 	//ent, content := Portal.GetDIDObj(Portal.PortalCat[0x06][rand.Int63()%int64(len(Portal.PortalCat[0x06]))])
	// 	_, content := Portal.GetDIDObj(did)
	// 	object := content.(*ac.Dat_Texture)
	// 	_, _ = object.ToPNG()
	// 	// log.Printf("DumpDID(0x%08X): %s\n\t%s", ent.GID, ent.ToString(&Portal), hdr)
	// }
	// SpellBaseHash_Parse("C:\\gdle\\Data\\json")
	// SpellBaseHash_Stats()
	// Weenie_Parse("C:\\gdle\\Data\\json")
	// Weenie_Stats()
	// SpawnMap_Parse("C:\\gdle\\Data\\json")
	// SpawnMap_Stats()
	// Weenie_Dump(30000531)
	// Weenie_Dump(31000596)
	// Weenie_Dump(31000589)
	// Weenie_Dump(31000401)
	// OpenDat("C:\\gdle\\Data\\client_portal.dat", PortalDat)
	// Portal.Parse("C:\\Projects\\")
	// Portal.Stats()

	// unknownVals := make(map[uint32]int)
	// for _, did := range Portal.PortalCat[6] {
	// 	_, content := Portal.GetDIDObj(did)
	// 	object := content.(*Dat_Texture)
	// 	unknownVals[object.Unknown]++
	// 	formatVals[object.Format]++
	// 	//log.Printf("DumpDID(0x%08X): %s\n\t%s", did, ent.ToString(&Portal), ToJSON(object))
	// }
	// log.Printf("unknownVals:%#v, formatVals:%#v", unknownVals, formatVals)
	//formatVals := make(map[uint32]int)
	// unknownVals = make(map[uint32]int)
	// formatVals = make(map[uint32]int)
	// for _, did := range HighRes.PortalCat[6] {
	// 	_, content := HighRes.GetDIDObj(did)
	// 	object := content.(*Dat_Texture)
	// 	unknownVals[object.Unknown]++
	// 	formatVals[object.Format]++
	// 	//log.Printf("DumpDID(0x%08X): %s\n\t%s", did, ent.ToString(&Portal), ToJSON(object))
	// }
	// log.Printf("unknownVals:%#v, formatVals:%#v", unknownVals, formatVals)
	//2023/01/18 23:07:37 Total PortalCat 0x06: 2294: [06003797 0600379C 060037A4 060037BA 060037C3 06003801 06003803 06003805 06003867 06003869] ... [060072D3 060072D5 060073A7 060073A8 060073A9 060073AA 060073AB 060073AC 060073AD 060073AE]

	// LocalEnglish.Parse("C:\\Projects\\")
	// LocalEnglish.Stats()
	// Portal.DumpDID(0xFFFF0001)
	// Cell.DumpDID(0xFFFF0001)
	// HighRes.DumpDID(0xFFFF0001)
	// LocalEnglish.DumpDID(0xFFFF0001)
	//Cell.DumpJSON(1491533823)
	//Cell.DumpJSON(0x0001FFFF)FFFF0001
	//Cell.DumpDID(0xFA220119)
	// Cell.DumpDID(0x6BE5FFFF)
	// Cell.DumpDID(0x6BE6FFFF)
	// Cell.DumpDID(0x6CE5FFFF)
	// Cell.DumpDID(0x6CE6FFFF)
	//log.Printf("cell:\n%s", ToJSON(&Cell))
	// Cell.DumpDID(0x58E6FFFF)
	// Portal.DumpDID(0x06001B21)
	// Portal.DumpDID(0x0E000004)
}
