package ac

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
)

var SpawnMaps_m sync.Mutex
var SpawnMaps = make(map[int]*SpawnMap)
var SpawnMaps_worldspawns int = 0
var SpawnMaps_worldspawnsreplacements int = 0
var SpawnMap_IDs = make([]int, 0)

func SpawnMap_Stats() {
	fmt.Printf("Total SpawnMaps: %d == %d (%d from worldspawns.json, %d of those replaced), 0x%08X thru 0x%08X\n", len(SpawnMaps), len(SpawnMap_IDs), SpawnMaps_worldspawns, SpawnMaps_worldspawnsreplacements, SpawnMap_IDs[0], SpawnMap_IDs[len(SpawnMap_IDs)-1])
}

func SpawnMap_Parse(root string) error {
	Weenie_m.Lock()
	defer Weenie_m.Unlock()
	SpawnMaps = make(map[int]*SpawnMap)
	SpawnMap_IDs = make([]int, 0)
	SpawnMaps_worldspawns = 0
	parseWorldSpawns(root)
	recurseParseSpawnMap(filepath.Join(root, "spawnMaps"))
	sort.Ints(SpawnMap_IDs)

	return nil
}
func recurseParseSpawnMap(path string) {
	edir, err := os.ReadDir(path)
	if err != nil {
		log.Printf("Could not open \"%s\" folder: %v", path, err)
		return
	}
	for _, e := range edir {
		info, _ := e.Info()
		file := path + "/" + e.Name()
		if info.IsDir() {
			recurseParseSpawnMap(file)
			continue
		}
		if info.Size() == 0 {
			continue
		}
		if !strings.HasSuffix(file, ".json") {
			continue
		}
		content, err := os.ReadFile(file)
		if err == nil {
			var w SpawnMap
			if err := json.Unmarshal(content, &w); err != nil {
				log.Printf("ERROR: failed to parse %s: %v", file, err)
				continue
			}
			w.Source = file
			if previous, ok := SpawnMaps[w.Key]; ok {
				if !strings.HasSuffix(previous.Source, "\\worldspawns.json") {
					log.Printf("ERROR: duplicate spawnMap %#04X %s", w.Key>>16, file)
					continue
				} else {
					SpawnMaps_worldspawnsreplacements++
					SpawnMaps[w.Key] = &w
				}
			} else {
				SpawnMaps[w.Key] = &w
				SpawnMap_IDs = append(SpawnMap_IDs, w.Key)
			}

			//fmt.Printf("loaded %s: %#04X, %d links, %d weenies\n", file, w.Key>>16, len(w.Value.Links), len(w.Value.Weenies))

		} else {
			fmt.Printf("ERROR: Unable to read spawnMap %s: %s\n", file, err)
		}
	}
}
func parseWorldSpawns(root string) {
	file := filepath.Join(root, "worldspawns.json")
	content, err := os.ReadFile(file)
	if err == nil {
		var w WorldSpawns
		if err := json.Unmarshal(content, &w); err != nil {
			log.Printf("ERROR: failed to parse %s: %v", file, err)
			return
		}
		SpawnMaps_worldspawns = len(w.LandBlocks)
		fmt.Printf("%s Version %s, %d landblocks.\n", file, w.Version, SpawnMaps_worldspawns)
		for _, spawnMap := range w.LandBlocks {
			spawnMap.Source = file
			SpawnMaps[spawnMap.Key] = &spawnMap
			SpawnMap_IDs = append(SpawnMap_IDs, spawnMap.Key)
		}
	} else {
		fmt.Printf("ERROR: Unable to read worldspawns.json(%s): %s\n", file, err)
	}
}

type SpawnMap struct {
	Source string `json:"-"`
	Key    int    `json:"key"`
	Value  struct {
		Links []struct {
			Target int `json:"target"`
			Source int `json:"source"`
		} `json:"links"`
		Weenies []struct {
			ID   int      `json:"id"`
			Wcid int      `json:"wcid"`
			Desc string   `json:"desc"`
			Pos  Position `json:"pos"`
		} `json:"weenies"`
	} `json:"value"`
	general
}
type WorldSpawns struct {
	Version    string     `json:"_version"`
	LandBlocks []SpawnMap `json:"landblocks"`
}
