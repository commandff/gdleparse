package ac

import (
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

type CPhatDataBinEntry struct {
	FileID   uint32 `json:"file_id"`
	Magic1   uint32 `json:"magic_1"`
	Magic2   uint32 `json:"magic_2"`
	Magic3   uint32 `json:"magic_3"`
	FMagic1  uint32 `json:"final_magic_1"`
	FMagic2  uint32 `json:"final_magic_2"`
	SegName  string `json:"segment_name"`
	DataType byte   `json:"data_type"`
	CLen     uint32 `json:"clen"`
	Len      uint32 `json:"len"`
	Data     []byte `json:"-"`
}

func (t *CPhatDataBinEntry) Read(buf []byte, fid uint32) int {
	t.FileID = binary.LittleEndian.Uint32(buf[0:])
	t.Magic1 = binary.LittleEndian.Uint32(buf[4:])
	t.Magic2 = binary.LittleEndian.Uint32(buf[8:])
	t.Magic3 = binary.LittleEndian.Uint32(buf[12:])
	t.DataType = buf[16]
	// fmt.Printf("%d, 0x%08x, 0x%08x, 0x%08x\n", fid, t.Magic1, t.Magic2, t.Magic3)

	var pos int = 17
	if t.DataType == 1 {
		t.CLen = binary.LittleEndian.Uint32(buf[pos:])
		t.Data = buf[pos+4 : pos+4+int(t.CLen)]
		t.Len = binary.LittleEndian.Uint32(buf[pos+4+int(t.CLen):])
		pos += 8 + int(t.CLen)

		b := bytes.NewReader(t.Data)
		r, err := zlib.NewReader(b)
		if err != nil {
			return pos
		}
		var res bytes.Buffer
		io.Copy(&res, r)
		r.Close()
		t.Data = res.Bytes()

		packing := t.Magic1
		fpacking := t.FMagic1
		for i := uint32(0); i < uint32(len(t.Data)>>2); i++ {
			packing = t.Magic3 + (t.Magic2 ^ packing)
			intpacking := binary.LittleEndian.Uint32(t.Data[i<<2:])
			intpacking += ((i + 1) * (packing + fpacking))
			t.Data[i<<2+0] = byte(intpacking)
			t.Data[i<<2+1] = byte(intpacking >> 8)
			t.Data[i<<2+2] = byte(intpacking >> 16)
			t.Data[i<<2+3] = byte(intpacking >> 24)
			fpacking ^= t.FMagic2
		}
		t.Len = binary.LittleEndian.Uint32(t.Data)
		ba := bytes.NewReader(t.Data[4:])
		ra, err := zlib.NewReader(ba)
		if err != nil {
			return pos
		}
		var res2 bytes.Buffer
		io.Copy(&res2, ra)
		ra.Close()
		t.Data = res2.Bytes()

	}
	//fmt.Printf("CPhatDataBinEntry %d: %s len %d:\n%s\n", fid, ToJSON(t), len(t.Data), hex.Dump(t.Data[0:min(64, len(t.Data)-1)]))
	return pos
}

type CPhatDataBin struct {
	FileName   string
	FileSize   uint32
	Version    uint32
	NumEntries uint32
	Entries    []CPhatDataBinEntry
	Seg1       RegionData
	Seg2       SpellTable
	// Seg3       TreasureTableData
	// Seg4       CraftTableData
	// Seg5       HousePortalDests
	// Seg6       InferredCellData
	// Seg7       AvatarTable
	// Seg8       QuestDefDB
	// Seg9       CWeenieDefaults
	// Seg10      MutationTable
	// Seg11      GameEvents
}

func (d *CPhatDataBin) Load(filepath string) bool {
	timeStart := time.Now()
	d.FileName = filepath

	fileData, err := os.ReadFile(d.FileName)
	if err != nil {
		log.Printf("CPhatDataBin::Load(%s) read err: %v", filepath, err)
		return false
	}
	d.FileSize = uint32(len(fileData))
	xor_val := d.FileSize + (d.FileSize << 15)
	for i := uint32(0); i < (d.FileSize - 3); i += 4 {
		fileData[i] ^= byte(xor_val)
		fileData[i+1] ^= byte(xor_val >> 8)
		fileData[i+2] ^= byte(xor_val >> 16)
		fileData[i+3] ^= byte(xor_val >> 24)
		xor_val <<= 3
		xor_val += d.FileSize
	}
	d.Version = binary.LittleEndian.Uint32(fileData[0:])
	d.NumEntries = binary.LittleEndian.Uint32(fileData[4:])

	fmt.Printf("File: %s version: %d size: %d num entries: %d\n", filepath, d.Version, d.FileSize, d.NumEntries)
	//os.WriteFile("cache_dec.bin", fileData, 0600)
	var pos int = 8
	// fancy foots-works
	for n := uint32(len(d.Entries)); n < d.NumEntries; n++ {
		d.Entries = append(d.Entries, CPhatDataBinEntry{})
		log.Printf("Appended additional entry?\n")
	}

	for i := uint32(0); i < d.NumEntries; i++ {
		data_id := binary.LittleEndian.Uint32(fileData[pos:])
		pos += 4
		len := d.Entries[i].Read(fileData[pos:], data_id)
		pos += len
	}
	fmt.Printf("cache.bin processed %d segments in %s\n", d.NumEntries, time.Since(timeStart))
	// fmt.Printf("proof: %s\n%s\n", ToJSON(d.Entries[1]), hex.Dump(d.Entries[1].Data[0:384]))

	d.Seg1.Read(&SBuffer{(bytes.NewBuffer(d.Entries[0].Data))})
	fmt.Printf("seg1: read %v\n", d.Seg1)
	d.Seg2.Read(&SBuffer{(bytes.NewBuffer(d.Entries[1].Data))})
	fmt.Printf("seg2: read %v\n", d.Seg2)
	// readb = d.Seg3.Read(d.Entries[2].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[2].Len)
	// readb = d.Seg4.Read(d.Entries[3].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[3].Len)
	// readb = d.Seg5.Read(d.Entries[4].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[4].Len)
	// readb = d.Seg6.Read(d.Entries[5].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[5].Len)
	// readb = d.Seg7.Read(d.Entries[6].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[6].Len)
	// readb = d.Seg8.Read(d.Entries[7].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[7].Len)
	// readb = d.Seg9.Read(d.Entries[8].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[8].Len)
	// readb = d.Seg10.Read(d.Entries[9].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[9].Len)
	// readb = d.Seg11.Read(d.Entries[10].Data)
	// fmt.Printf("seg3: read %d of %d\n", readb, d.Entries[10].Len)
	return false
}

// RegionData
type RegionData struct {
	TotalObjects       uint32
	NumEncounterTables uint32
	EncounterTables    []EncounterTable
	EncounterMap       []byte
}

func (t *RegionData) Read(s *SBuffer) *RegionData {
	t.TotalObjects = *s.readUint32()
	t.NumEncounterTables = *s.readUint32()
	t.EncounterTables = make([]EncounterTable, int(t.NumEncounterTables))
	for i := range t.EncounterTables {
		t.EncounterTables[i].Read(s)
	}
	t.EncounterMap = s.Next(255 * 255)
	return t
}

type EncounterTable struct {
	Index  uint32
	Values []uint32
}

func (t *EncounterTable) Read(s *SBuffer) *EncounterTable {
	t.Index = *s.readUint32()
	t.Values = *s.readUint32s(16)
	return t
}
