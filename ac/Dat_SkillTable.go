package ac

type Dat_SkillTable struct {
	Id            uint32               `json:"id"`
	TotalObjects  uint16               `json:"total_objects"`
	BucketSize    uint16               `json:"bucket_size"`
	SkillBaseHash []Dat_SkillHashEntry `json:"skill_base_hash"`
}

func (t *Dat_SkillTable) Read(s *SBuffer) *Dat_SkillTable {
	t.Id = *s.readUint32()
	t.TotalObjects = *s.readUint16()
	t.BucketSize = *s.readUint16()
	t.SkillBaseHash = make([]Dat_SkillHashEntry, int(t.TotalObjects))
	for i := range t.SkillBaseHash {
		t.SkillBaseHash[i].Read(s)
	}
	return t
}

type Dat_SkillHashEntry struct {
	Key   uint32        `json:"key"`
	Value Dat_SkillBase `json:"value"`
}

func (t *Dat_SkillHashEntry) Read(s *SBuffer) *Dat_SkillHashEntry {
	t.Key = *s.readUint32()
	t.Value.Read(s)
	return t
}

type Dat_SkillBase struct {
	Description     Dat_PString      `json:"description"`
	Name            Dat_PString      `json:"name"`
	IconId          uint32           `json:"icon_id"`
	TrainedCost     int32            `json:"trained_cost"`
	SpecializedCost int32            `json:"specialized_cost"`
	Category        uint32           `json:"category"`
	ChargenUse      uint32           `json:"chargen_use"`
	MinLevel        uint32           `json:"min_level"`
	Formula         Dat_SkillFormula `json:"formula"`
	UpperBound      float64          `json:"upper_bound"`
	LowerBound      float64          `json:"lower_bound"`
	LearnMod        float64          `json:"learn_mod"`
}

func (t *Dat_SkillBase) Read(s *SBuffer) *Dat_SkillBase {
	t.Description.Read(s, false)
	t.Name.Read(s, false)
	t.IconId = *s.readUint32()
	t.TrainedCost = *s.readInt32()
	t.SpecializedCost = *s.readInt32()
	t.Category = *s.readUint32()
	t.ChargenUse = *s.readUint32()
	t.MinLevel = *s.readUint32()
	t.Formula.Read(s)
	t.UpperBound = *s.readDouble()
	t.LowerBound = *s.readDouble()
	t.LearnMod = *s.readDouble()
	return t
}

type Dat_SkillFormula struct {
	W     uint32        `json:"w"`
	X     uint32        `json:"x"`
	Y     uint32        `json:"y"`
	Z     uint32        `json:"z"`
	Attr1 Dat_Attribute `json:"attr1"`
	Attr2 Dat_Attribute `json:"attr2"`
}

func (t *Dat_SkillFormula) Read(s *SBuffer) *Dat_SkillFormula {
	t.W = *s.readUint32()
	t.X = *s.readUint32()
	t.Y = *s.readUint32()
	t.Z = *s.readUint32()
	t.Attr1 = Dat_Attribute(*s.readUint32())
	t.Attr2 = Dat_Attribute(*s.readUint32())
	return t
}
