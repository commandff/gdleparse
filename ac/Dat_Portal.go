package ac

// //////////////////////////////////////////////////////////////
// Setup 0x02
// //////////////////////////////////////////////////////////////
type Dat_Setup struct {
	Id                  uint32                        `json:"id"`
	Flags               uint32                        `json:"flags"`
	NumParts            uint32                        `json:"num_Parts"`
	Parts               []uint32                      `json:"parts"`
	ParentIndex         []uint32                      `json:"parent_index"`
	DefaultScale        []Vector3                     `json:"default_scale"`
	NumHoldingLocations uint32                        `json:"num_holding_locations"`
	HoldingLocations    map[uint32]*Dat_LocationType  `json:"holding_locations"`
	NumConnectionPoints uint32                        `json:"num_connection_points"`
	ConnectionPoints    map[uint32]*Dat_LocationType  `json:"connection_points"`
	NumPlacementFrames  uint32                        `json:"num_placement_frames"`
	PlacementFrames     map[uint32]*Dat_PlacementType `json:"placement_frames"`
	NumCylSpheres       uint32                        `json:"num_cyl_spheres"`
	CylSpheres          []Dat_CylSphere               `json:"cyl_spheres"`
	NumSpheres          uint32                        `json:"num_spheres"`
	Spheres             []Dat_Sphere                  `json:"spheres"`
	Height              float32                       `json:"height"`
	Radius              float32                       `json:"radius"`
	StepUpHeight        float32                       `json:"step_up_height"`
	StepDownHeight      float32                       `json:"step_down_height"`
	SortingSphere       Dat_Sphere                    `json:"sorting_sphere"`
	SelectionSphere     Dat_Sphere                    `json:"selection_sphere"`
	NumLights           uint32                        `json:"num_lights"`
	Lights              map[uint32]*Dat_LightInfo     `json:"lights"`
	DefaultAnimation    uint32                        `json:"default_animation"`
	DefaultScript       uint32                        `json:"default_script"`
	DefaultMotionTable  uint32                        `json:"default_motion_table"`
	DefaultSoundTable   uint32                        `json:"default_sound_table"`
	DefaultScriptTable  uint32                        `json:"default_script_table"`
}

func (t *Dat_Setup) Read(s *SBuffer) *Dat_Setup {
	t.Id = *s.readUint32()
	t.Flags = *s.readUint32()
	t.NumParts = *s.readUint32()
	t.Parts = *s.readUint32s(int(t.NumParts))
	if (t.Flags & 1) == 1 {
		t.ParentIndex = *s.readUint32s(int(t.NumParts))
	}
	if (t.Flags & 2) == 2 {
		t.DefaultScale = make([]Vector3, int(t.NumParts))
		for i := range t.DefaultScale {
			t.DefaultScale[i].Read(s)
		}
	}
	t.NumHoldingLocations = *s.readUint32()
	t.HoldingLocations = make(map[uint32]*Dat_LocationType)
	for i := 0; i < int(t.NumHoldingLocations); i++ {
		key := *s.readUint32()
		t.HoldingLocations[key] = new(Dat_LocationType).Read(s)
	}
	t.NumConnectionPoints = *s.readUint32()
	t.ConnectionPoints = make(map[uint32]*Dat_LocationType)
	for i := 0; i < int(t.NumConnectionPoints); i++ {
		key := *s.readUint32()
		t.ConnectionPoints[key] = new(Dat_LocationType).Read(s)
	}
	t.NumPlacementFrames = *s.readUint32()
	t.PlacementFrames = make(map[uint32]*Dat_PlacementType)
	for i := 0; i < int(t.NumPlacementFrames); i++ {
		key := *s.readUint32()
		t.PlacementFrames[key] = new(Dat_PlacementType).Read(s, int(t.NumParts))
	}
	t.NumCylSpheres = *s.readUint32()
	t.CylSpheres = make([]Dat_CylSphere, int(t.NumCylSpheres))
	for i := range t.CylSpheres {
		t.CylSpheres[i].Read(s)
	}
	t.NumSpheres = *s.readUint32()
	t.Spheres = make([]Dat_Sphere, int(t.NumSpheres))
	for i := range t.Spheres {
		t.Spheres[i].Read(s)
	}
	t.Height = *s.readSingle()
	t.Radius = *s.readSingle()
	t.StepUpHeight = *s.readSingle()
	t.StepDownHeight = *s.readSingle()
	t.SortingSphere.Read(s)
	t.SelectionSphere.Read(s)

	t.NumLights = *s.readUint32()

	t.Lights = make(map[uint32]*Dat_LightInfo)
	for i := 0; i < int(t.NumLights); i++ {
		key := *s.readUint32()

		t.Lights[key] = new(Dat_LightInfo)
		t.Lights[key].Read(s)
	}

	t.DefaultAnimation = *s.readUint32()
	t.DefaultScript = *s.readUint32()
	t.DefaultMotionTable = *s.readUint32()
	t.DefaultSoundTable = *s.readUint32()
	t.DefaultScriptTable = *s.readUint32()

	return t
}

// //////////////////////////////////////////////////////////////
// Palette 0x04
// //////////////////////////////////////////////////////////////
type Dat_Palette struct {
	Id        uint32   `json:"id"`
	NumColors uint32   `json:"num_colors"`
	Colors    []uint32 `json:"colors"`
}

func (t *Dat_Palette) Read(s *SBuffer) *Dat_Palette {
	t.Id = *s.readUint32()
	t.NumColors = *s.readUint32()
	t.Colors = *s.readUint32s(int(t.NumColors))
	return t
}

// //////////////////////////////////////////////////////////////
// PaletteSet 0x0F
// //////////////////////////////////////////////////////////////
type Dat_PaletteSet struct {
	Id          uint32   `json:"id"`
	NumPalettes uint32   `json:"num_palettes"`
	Palettes    []uint32 `json:"palettes"`
}

func (t *Dat_PaletteSet) Read(s *SBuffer) *Dat_PaletteSet {
	t.Id = *s.readUint32()
	t.NumPalettes = *s.readUint32()
	t.Palettes = *s.readUint32s(int(t.NumPalettes))
	return t
}
func (t *Dat_PaletteSet) GetPaletteId(shade float64) uint32 {
	if t.NumPalettes == 0 || shade < 0.0 || shade > 1.0 {
		return 0
	}
	var idx int = int((float64(t.NumPalettes) - 0.000001) * shade)
	if idx < 0 {
		idx = 0
	}
	if idx > int(t.NumPalettes-1) {
		idx = int(t.NumPalettes - 1)
	}
	return t.Palettes[idx]
}
