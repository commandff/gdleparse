package ac

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"unsafe"
)

type DatFile struct {
	M              sync.Mutex          `json:"-"`
	Filename       string              `json:"file_name"`
	LoadedPath     string              `json:"loaded_path"`
	Dat            map[uint32]*BTEntry `json:"-"` // `json:"dat"`
	Tree           map[uint32]*BTNode  `json:"-"`
	DIDs           []uint32            `json:"-"` // `json:"dids"`
	LandBlocks     []uint32            `json:"-"` // `json:"landblocks"`
	LandBlockInfos []uint32            `json:"-"` // `json:"landblockinfos"`
	EnvCells       []uint32            `json:"-"` // `json:"envcells"`
	PortalCat      map[uint32][]uint32 `json:"-"` // `json:"portal_categories"`
	F              *os.File            `json:"-"`
	Header         *DiskFileInfo_t     `json:"header"`
	Iteration      Dat_Iteration       `json:"iteration"`
}

func (dat *DatFile) recurseBTree(offset uint32) error {
	thisNode := new(BTNode)
	dat.Tree[offset] = thisNode
	buf := dat.ReadDat(offset, int(unsafe.Sizeof(*thisNode)))
	if buf.Len() == 0 {
		return fmt.Errorf("failed to Fetch node %d %s", offset, dat.LoadedPath)
	}
	err := binary.Read(buf, binary.LittleEndian, thisNode)
	if err != nil {
		return fmt.Errorf("failed to Decode node 0x%08X %s: %v -- Read %db from %s as Node expected %d", offset, dat.LoadedPath, err, buf.Len(), dat.LoadedPath, unsafe.Sizeof(*thisNode))
	}

	//iterate child nodes
	for i, nn := range thisNode.NextNode {
		if i >= int(thisNode.NumEntries)+1 {
			break
		}

		if nn == 0 || nn == 0xCDCDCDCD {
			break
		}

		if _, ok := dat.Tree[nn]; !ok {
			err := dat.recurseBTree(nn)
			if err != nil {
				log.Println(err)
				break
			}
		}
	}
	//iterate file entries in this node
	for i, ent := range thisNode.Entries {
		if uint32(i) >= thisNode.NumEntries {
			break
		}
		if _, ok := dat.Dat[ent.GID]; !ok {
			if ent.GID == 0xFFFF0001 {
				dat.Iteration.Read(dat.ReadDat(ent.Offset, int(ent.Size)))
				continue
			}
			dat.Dat[ent.GID] = &thisNode.Entries[i] // add file entry
			switch dat.Header.DataSetLM {
			case 2: // "CELL_DATFILE"
				if (ent.GID & 0x0000FFFF) == 0x0000FFFF {
					dat.LandBlocks = append(dat.LandBlocks, ent.GID)
				} else if (ent.GID & 0x0000FFFF) == 0x0000FFFE {
					dat.LandBlockInfos = append(dat.LandBlockInfos, ent.GID)
				} else {
					dat.EnvCells = append(dat.EnvCells, ent.GID)
				}
			case 0: // "UNDEF_DISK"
				fallthrough
			case 3: // "LOCAL_DATFILE"
				fallthrough
			case 1: // "PORTAL_DATFILE"
				fallthrough
			default:
				dat.PortalCat[ent.GID>>24] = append(dat.PortalCat[ent.GID>>24], ent.GID)
			}
			dat.DIDs = append(dat.DIDs, ent.GID)
		}
	}
	return nil
}

// returns pointer to entry, and `any` object of contents.
// returns nil, nil on not found.
func (dat *DatFile) GetDIDObj(did uint32) (*BTEntry, any) {
	if obj, ok := dat.Dat[did]; ok {
		buf := dat.ReadDat(obj.Offset, int(obj.Size))
		if did == 0xFFFF0001 {
			thisDat_Iteration := new(Dat_Iteration)
			thisDat_Iteration.Read(buf)
			return obj, thisDat_Iteration
		}
		switch dat.Header.DataSetLM {
		case 2: // "CELL_DATFILE"
			switch obj.GID & 0xFFFF {
			case 0xFFFF:
				thisLandBlock := new(Dat_LandBlock)
				thisLandBlock.Read(buf)
				return obj, thisLandBlock
			case 0xFFFE:
				thisLandBlockInfo := new(Dat_LandBlockInfo)
				thisLandBlockInfo.Read(buf)
				return obj, thisLandBlockInfo
			default:
				thisEnvCell := new(Dat_EnvCell)
				thisEnvCell.Read(buf)
				return obj, thisEnvCell
			}
		case 0: // "UNDEF_DISK"
			fallthrough
		case 3: // "LOCAL_DATFILE"
			fallthrough
		case 1: // "PORTAL_DATFILE"
			fallthrough
		default:
			switch obj.GID >> 24 {
			case 0x00: // ???
			case 0x01: // GraphicsObject
			case 0x02: // Setup
				thisSetup := new(Dat_Setup)
				thisSetup.Read(buf)
				return obj, thisSetup
			case 0x03: // Animation
				thisAnimation := new(Dat_Animation)
				thisAnimation.Read(buf)
				return obj, thisAnimation
			case 0x04: // Palette
				thisPalette := new(Dat_Palette)
				thisPalette.Read(buf)
				return obj, thisPalette
			case 0x05: // SurfaceTexture
			case 0x06: // Texture
				thisTexture := new(Dat_Texture)
				thisTexture.Read(buf)
				return obj, thisTexture
			case 0x07: // ???
			case 0x08: // Surface
			case 0x09: // MotionTable
			case 0x0A: // Wave
			case 0x0B: // ???
			case 0x0C: // ???
			case 0x0D: // Environment
			case 0x0E: // Tables
				switch obj.GID >> 16 {
				case 0x0E01: // QualityFilter
				case 0x0E02: // MonitoredProperties
				case 0x0E00: // ClientTable
					switch obj.GID {
					case 0x0E000000: //
					case 0x0E000001: //
					case 0x0E000002: // CharacterGenerator
					case 0x0E000003: // SecondaryAttributeTable
					case 0x0E000004: // SkillTable
						thisSkillTable := new(Dat_SkillTable)
						thisSkillTable.Read(buf)
						return obj, thisSkillTable
					case 0x0E000005: //
					case 0x0E000006: //
					case 0x0E000007: // ChatPoseTable
					case 0x0E000008: //
					case 0x0E000009: //
					case 0x0E00000A: //
					case 0x0E00000B: //
					case 0x0E00000C: //
					case 0x0E00000D: // ObjectHierarchy
					case 0x0E00000E: // SpellTable
					case 0x0E00000F: // SpellComponentTable
					case 0x0E000010: //
					case 0x0E000011: //
					case 0x0E000012: //
					case 0x0E000013: //
					case 0x0E000014: //
					case 0x0E000015: //
					case 0x0E000016: //
					case 0x0E000017: //
					case 0x0E000018: // XpTable
					case 0x0E000019: //
					case 0x0E00001A: // BadData
					case 0x0E00001B: //
					case 0x0E00001C: //
					case 0x0E00001D: // ContractTable
					case 0x0E00001E: // TabooTable
					case 0x0E00001F: // FileToId
					case 0x0E000020: // NameFilterTable
					}

				}
			case 0x0F:
				thisPaletteSet := new(Dat_PaletteSet)
				thisPaletteSet.Read(buf)
				return obj, thisPaletteSet
			case 0x10: // Clothing
			case 0x11: // DegradeInfo
			case 0x12: // Scene
			case 0x13: // Region
			case 0x14: // KeyMap
			case 0x15: // RenderTexture
			case 0x16: // RenderMaterial
			case 0x17: // MaterialModifier
			case 0x18: // MaterialInstance
			case 0x19: //
			case 0x1A: //
			case 0x1B: //
			case 0x1C: //
			case 0x1D: //
			case 0x1E: //
			case 0x1F: //
			case 0x20: // SoundTable
			case 0x21: // UiLayout
			case 0x22: // EnumMapper
			case 0x23: // StringTable
			case 0x24: //
			case 0x25: // DidMapper
			case 0x26: // ActionMap
			case 0x27: // DualDidMapper
			case 0x28: //
			case 0x29: //
			case 0x2A: //
			case 0x2B: //
			case 0x2C: //
			case 0x2D: //
			case 0x2E: //
			case 0x2F: //
			case 0x30: // CombatTable
			case 0x31: // String
			case 0x32: // ParticleEmitter
			case 0x33: // PhysicsScript
			case 0x34: // PhysicsScriptTable
			case 0x35: //
			case 0x36: //
			case 0x37: //
			case 0x38: //
			case 0x39: // MasterProperty
			case 0x40: // Font
			case 0x41: // StringState
			case 0x78: // DbProperties
			}
		}
		return obj, buf
	} else {
		return nil, nil
	}
}

// returns pointer to entry, and json string of contents
func (dat *DatFile) GetDID(did uint32) (*BTEntry, string) {
	obj, content := dat.GetDIDObj(did)
	if obj == nil {
		return nil, "{}"
	}
	return obj, ToJSON(content)
}

// Testing Func- Dumps DID to log
func (dat *DatFile) DumpDID(did uint32) {
	obj, content := dat.GetDID(did)
	if obj == nil {
		log.Printf("DumpDID(0x%08X): No Match", did)
		return
	}
	log.Printf("DumpDID(0x%08X): %s\n\t%s", did, obj.ToString(dat), content)
}

func (dat *DatFile) Stats() {
	log.Printf("Dat Stats %s: %s\n", dat.LoadedPath, ToJSON(dat.Header))
	if len(dat.Dat) == 0 {
		log.Printf("Total DIDs: %d\n", len(dat.DIDs))
	} else if len(dat.Dat) < 20 {
		log.Printf("Total DIDs: %d: %08X\n", len(dat.DIDs), dat.DIDs)
	} else {
		log.Printf("Total DIDs: %d: %08X ... %08X\n", len(dat.DIDs), dat.DIDs[0:10], dat.DIDs[len(dat.DIDs)-10:])
	}
	if len(dat.LandBlocks) > 0 {
		if len(dat.LandBlocks) < 20 {
			log.Printf("Total LandBlocks: %d: %08X\n", len(dat.LandBlocks), dat.LandBlocks)
		} else {
			log.Printf("Total LandBlocks: %d: %08X ... %08X\n", len(dat.LandBlocks), dat.LandBlocks[0:10], dat.LandBlocks[len(dat.LandBlocks)-10:])
		}
	}
	if len(dat.LandBlockInfos) > 0 {
		if len(dat.LandBlockInfos) < 20 {
			log.Printf("Total LandBlockInfos: %d: %08X\n", len(dat.LandBlockInfos), dat.LandBlockInfos)
		} else {
			log.Printf("Total LandBlockInfos: %d: %08X ... %08X\n", len(dat.LandBlockInfos), dat.LandBlockInfos[0:10], dat.LandBlockInfos[len(dat.LandBlockInfos)-10:])
		}
	}
	if len(dat.EnvCells) > 0 {
		if len(dat.EnvCells) < 20 {
			log.Printf("Total EnvCells: %d: %08X\n", len(dat.EnvCells), dat.EnvCells)
		} else {
			log.Printf("Total EnvCells: %d: %08X ... %08X\n", len(dat.EnvCells), dat.EnvCells[0:10], dat.EnvCells[len(dat.EnvCells)-10:])
		}
	}
	for q := uint32(0); q < 256; q++ {
		if c, ok := dat.PortalCat[q]; ok {
			if len(c) == 0 {
				log.Printf("Total PortalCat 0x%02X: %d\n", q, len(c))
			} else if len(c) < 20 {
				log.Printf("Total PortalCat 0x%02X: %d: %08X\n", q, len(c), c)
			} else {
				log.Printf("Total PortalCat 0x%02X: %d: %08X ... %08X\n", q, len(c), c[0:10], c[len(c)-10:])
			}
		}
	}
}
func (dat *DatFile) Parse(root string) error {
	var err error

	dat.M.Lock()
	defer dat.M.Unlock()
	dat.Dat = make(map[uint32]*BTEntry)
	dat.Tree = make(map[uint32]*BTNode)
	dat.DIDs = make([]uint32, 0)
	dat.LandBlocks = make([]uint32, 0)
	dat.LandBlockInfos = make([]uint32, 0)
	dat.EnvCells = make([]uint32, 0)
	dat.PortalCat = make(map[uint32][]uint32, 0)
	dat.Header = &DiskFileInfo_t{}

	dat.LoadedPath = filepath.Join(root, dat.Filename)
	dat.F, err = os.Open(dat.LoadedPath)
	if err != nil {
		return fmt.Errorf("failed to open %s: %v", dat.LoadedPath, err)
	}
	b := make([]byte, unsafe.Sizeof(*dat.Header))
	_, err = dat.F.ReadAt(b, 0x140)
	if err != nil {
		return fmt.Errorf("failed to Read Header %s: %v", dat.LoadedPath, err)
	}
	// log.Printf("Read %db from %s as Header", r, dat.LoadedPath)

	buf := bytes.NewBuffer(b)

	err = binary.Read(buf, binary.LittleEndian, dat.Header)
	if err != nil {
		return fmt.Errorf("failed to Decode Header %s: %v", dat.LoadedPath, err)
	}
	if dat.Header.Magic != 0x00005442 {
		return fmt.Errorf("ignored \"%s\": Invalid Header", dat.LoadedPath)
	}

	dat.recurseBTree(dat.Header.BTreeRoot)

	sort.SliceStable(dat.DIDs, func(i, j int) bool {
		return dat.DIDs[i] < dat.DIDs[j]
	})
	sort.SliceStable(dat.LandBlocks, func(i, j int) bool {
		return dat.LandBlocks[i] < dat.LandBlocks[j]
	})
	sort.SliceStable(dat.LandBlockInfos, func(i, j int) bool {
		return dat.LandBlockInfos[i] < dat.LandBlockInfos[j]
	})
	sort.SliceStable(dat.EnvCells, func(i, j int) bool {
		return dat.EnvCells[i] < dat.EnvCells[j]
	})
	for _, c := range dat.PortalCat {
		sort.SliceStable(c, func(i, j int) bool {
			return c[i] < c[j]
		})
	}

	return nil
}

func (dat *DatFile) ReadDat(offset uint32, size int) *SBuffer {
	var buffer []byte = make([]byte, size)
	var bufferOffset int = 0
	var datSize int = int(dat.Header.BlockSize) - 4
	for size > 0 {
		var nextAddress uint32 = dat.GetNextAddress(offset)
		offset += 4
		if size < datSize {
			_, err := dat.F.ReadAt(buffer[bufferOffset:bufferOffset+size], int64(offset))
			if err != nil {
				log.Printf("ReadDat(0x%08X,%d): Read Error: %s", offset, size, err)
				return nil
			}
			size = 0
		} else {
			_, err := dat.F.ReadAt(buffer[bufferOffset:(bufferOffset+datSize)], int64(offset))
			if err != nil {
				log.Printf("ReadDat(0x%08X,%d): Read Error: %s", offset, size, err)
				return nil
			}
			bufferOffset += datSize
			offset = nextAddress
			size -= datSize
		}
	}
	// this feels wrong. might be double-converting
	return &SBuffer{(bytes.NewBuffer(buffer))}
}

func (dat *DatFile) GetNextAddress(offset uint32) uint32 {
	var buffer []byte = make([]byte, 4)
	n, err := dat.F.ReadAt(buffer, int64(offset))
	if err != nil {
		log.Printf("GetNextAddress(0x%08X): Read Error: %s", offset, err)
		return 0
	}
	if n != 4 {
		log.Printf("GetNextAddress(0x%08X): Read %d", offset, n)
		return 0
	}
	return binary.LittleEndian.Uint32(buffer)
}
