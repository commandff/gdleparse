package ac

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"strings"
)

type CWeenieSave struct {
	SaveVersion    uint32       `json:"save_version"`
	SaveTimestamp  uint32       `json:"save_timestamp"`
	SaveInstanceTS uint32       `json:"save_instance_timestamp"`
	Qualities      CACQualities `json:"qualities"`
	ObjDesc        ObjDesc      `json:"obj_desc"`
	WornObjDesc    ObjDesc      `json:"WornObjDesc"`

	Fields       *uint32           `json:"fields,omitempty"`
	PlayerModule *PlayerModule     `json:"PlayerModule,omitempty"`
	Equipment    *[]uint32         `json:"equipment,omitempty"`
	Inventory    *[]uint32         `json:"inventory,omitempty"`
	Packs        *[]uint32         `json:"packs,omitempty"`
	QuestTable   []QuestTableEntry `json:"questTable,omitempty"`
}

func (t *CWeenieSave) Read(buf []byte) *CWeenieSave {
	var s *SBuffer = &SBuffer{Buffer: (bytes.NewBuffer(buf))}

	t.SaveVersion = *s.readUint32()
	t.SaveTimestamp = *s.readUint32()
	t.SaveInstanceTS = *s.readUint32()
	t.Qualities.Read(s)
	t.ObjDesc.Read(s)
	if t.SaveVersion < 3 {
		s.readUint32s(3)
	}
	t.WornObjDesc.Read(s)
	if t.SaveVersion < 2 {
		return t
	}
	fields := *s.readUint32()
	if fields&1 != 0 {
		t.PlayerModule = new(PlayerModule).Read(s)
	}
	if fields&2 != 0 {
		t.Equipment = s.readUint32s(int(*s.readUint32() & 0xFFFF))
		t.Inventory = s.readUint32s(int(*s.readUint32() & 0xFFFF))
		t.Packs = s.readUint32s(int(*s.readUint32() & 0xFFFF))
	}
	if fields&4 != 0 {
		t.QuestTable = make([]QuestTableEntry, int(*s.readUint32()&0xFFFF))
		for i := range t.QuestTable {
			key := *new(Dat_PString).Read(s, false)
			t.QuestTable[i] = QuestTableEntry{Key: key, Value: *new(QuestProfile).Read(s)}
		}
	}
	return t
}

type ObjDesc struct {
	PaletteID   uint32             `json:"paletteID"`
	Subpalettes []Subpalette       `json:"subpalettes"`
	TMChanges   []TextureMapChange `json:"TMChanges"`
	APChanges   []AnimPartChange   `json:"APChanges"`
}

func (t *ObjDesc) Read(s *SBuffer) *ObjDesc {
	startLen := s.Len()
	numEleven := *s.readByte()
	if numEleven != 0x11 {
		return t
	}
	numSubpalettes := int(*s.readByte())
	numTMCs := int(*s.readByte())
	numAPCs := int(*s.readByte())
	t.Subpalettes = make([]Subpalette, numSubpalettes)
	for i := range t.Subpalettes {
		t.Subpalettes[i].Read(s)
	}
	t.TMChanges = make([]TextureMapChange, numTMCs)
	for i := range t.TMChanges {
		t.TMChanges[i].Read(s)
	}
	t.APChanges = make([]AnimPartChange, numAPCs)
	for i := range t.APChanges {
		t.APChanges[i].Read(s)
	}

	if (s.Len()-startLen)&2 == 2 { // very janky alignment.
		s.readUint16()
	}
	return t
}

type PlayerModule struct {
	ShortCuts         []ShortCutData       `json:"shortcuts"`
	FavoriteSpells    [][]uint32           `json:"favorite_spells"`
	DesiredComps      DesiredComps         `json:"desired_comps"`
	Options           uint32               `json:"options"`
	Options2          uint32               `json:"options2"`
	SpellFilters      uint32               `json:"spell_filters"`
	PlayerOptionsData GenericQualitiesData `json:"player_options_data"`
	TimeStampFormat   Dat_PString          `json:"TimeStampFormat"`
	// WindowData        []byte               `json:"windowData"`
}
type DesiredComps map[uint32]int32

func (t *DesiredComps) MarshalJSON() ([]byte, error) {
	var ret []string
	for i, e := range *t {
		ret = append(ret, fmt.Sprintf("{\"key\":%d,\"value\":%d}", i, e))
	}
	return []byte("[" + strings.Join(ret, ",") + "]"), nil
}
func (t *PlayerModule) Read(s *SBuffer) *PlayerModule {
	header := *s.readUint32()
	t.Options = *s.readUint32()
	if header&1 != 0 {
		t.ShortCuts = make([]ShortCutData, int(*s.readUint32()&0xFFFF))
		for i := range t.ShortCuts {
			t.ShortCuts[i].Read(s)
		}
	}

	if header&0x04 != 0 {
		t.FavoriteSpells = make([][]uint32, 5)
	} else if header&0x10 != 0 {
		t.FavoriteSpells = make([][]uint32, 7)
	} else if header&0x400 != 0 {
		t.FavoriteSpells = make([][]uint32, 8)
	} else {
		t.FavoriteSpells = make([][]uint32, 1)
	}

	for i := range t.FavoriteSpells {
		t.FavoriteSpells[i] = *s.readUint32s(int(*s.readUint32() & 0xFFFF))
	}
	if header&0x08 != 0 {
		desiredCompsLen := *s.readUint32() & 0xFFFF
		t.DesiredComps = make(DesiredComps)
		for i := 0; i < int(desiredCompsLen); i++ {
			key := *s.readUint32()
			t.DesiredComps[key] = *s.readInt32()
		}
	}

	if header&0x20 != 0 {
		t.SpellFilters = *s.readUint32()
	} else {
		t.SpellFilters = 0x3FFF
	}

	if header&0x40 != 0 {
		t.Options2 = *s.readUint32()
	} else {
		t.Options2 = 0x948700
	}

	if header&0x80 != 0 {
		t.TimeStampFormat.Read(s, false)
	}
	if header&0x100 != 0 {
		t.PlayerOptionsData.Read(s)
	}
	// if header&0x200 != 0 {
	// 	t.WindowData = s.readBytes(0)
	// }

	return t
}

type GenericQualitiesData struct {
	IntStats    []IntStat    `json:"intStats,omitempty"`    // STypeInt
	BoolStats   []BoolStat   `json:"boolStats,omitempty"`   // STypeBool
	FloatStats  []FloatStat  `json:"floatStats,omitempty"`  // STypeFloat
	StringStats []StringStat `json:"stringStats,omitempty"` // STypeString
}

func (t *GenericQualitiesData) Read(s *SBuffer) *GenericQualitiesData {
	header := *s.readUint32()
	if header&0x01 != 0 {
		t.IntStats = make([]IntStat, *s.readUint32()&0xFFFF)
		for i := range t.IntStats {
			key := *s.readUint32()
			t.IntStats[i] = IntStat{Key: key, Value: *s.readInt32()}
		}
	}
	if header&0x02 != 0 {
		t.BoolStats = make([]BoolStat, *s.readUint32()&0xFFFF)
		for i := range t.BoolStats {
			key := *s.readUint32()
			t.BoolStats[i] = BoolStat{Key: key, Value: byte(*s.readInt32())}
		}
	}
	if header&0x04 != 0 {
		t.FloatStats = make([]FloatStat, *s.readUint32()&0xFFFF)
		for i := range t.FloatStats {
			key := *s.readUint32()
			t.FloatStats[i] = FloatStat{Key: key, Value: *s.readDouble()}
		}
	}
	if header&0x08 != 0 {
		t.StringStats = make([]StringStat, *s.readUint32()&0xFFFF)
		for i := range t.StringStats {
			key := *s.readUint32()
			t.StringStats[i] = StringStat{Key: key, Value: *new(Dat_PString).Read(s, false)}
		}
	}
	return t
}

type ShortCutData struct {
	Index    int32  `json:"index"`
	ObjectID uint32 `json:"objectID"`
	SpellID  uint32 `json:"spellID"`
}

func (t *ShortCutData) Read(s *SBuffer) *ShortCutData {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type QuestTableEntry struct {
	Key   Dat_PString  `json:"key"`
	Value QuestProfile `json:"value"`
}

func (t *QuestTableEntry) Read(s *SBuffer) *QuestTableEntry {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type QuestProfile struct {
	Last_update     float64 `json:"last_update"`
	Real_time       int32   `json:"real_time"`
	Num_completions uint32  `json:"num_completions"`
}

func (t *QuestProfile) Read(s *SBuffer) *QuestProfile {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type CACQualities struct {
	CBaseQualities
	WCID              uint32                `json:"wcid"`
	Attributes        *AttributeCache       `json:"attributes,omitempty"`
	Skills            []SkillStat           `json:"skills,omitempty"` // STypeSkill
	Body              *Body                 `json:"body,omitempty"`
	SpellBook         []SpellBook           `json:"spellbook,omitempty"`
	Enchantments      *CEnchantmentRegistry `json:"enchantments,omitempty"`
	EventFilter       []Event               `json:"eventFilter,omitempty"`
	EmoteTable        []EmoteTable          `json:"emoteTable,omitempty"`
	CreateList        []CreationProfile     `json:"createList,omitempty"`
	PageDataList      *PageDataList         `json:"pageDataList,omitempty"`
	GeneratorTable    []Generator           `json:"generatorTable,omitempty"`
	GeneratorRegistry []GeneratorRegistry   `json:"generatorRegistry,omitempty"`
	GeneratorQueue    []GeneratorQueue      `json:"generatorQueue,omitempty"`
}

func (t *CACQualities) Read(s *SBuffer) *CACQualities {
	t.CBaseQualities.Read(s)
	contentFlags := *s.readUint32()
	t.WCID = *s.readUint32()
	if contentFlags&0x0001 != 0 {
		t.Attributes = new(AttributeCache).Read(s)
	}
	if contentFlags&0x0002 != 0 {
		t.Skills = make([]SkillStat, *s.readUint32()&0xFFFF)
		for i := range t.Skills {
			key := *s.readUint32()
			t.Skills[i] = SkillStat{Key: key, Value: *new(Skill).Read(s)}
		}
	}
	if contentFlags&0x0004 != 0 {
		t.Body = new(Body).Read(s)
	}
	if contentFlags&0x0100 != 0 {
		t.SpellBook = make([]SpellBook, *s.readUint32()&0xFFFF)
		for i := range t.SpellBook {
			t.SpellBook[i] = *new(SpellBook).Read(s)
		}
	}
	if contentFlags&0x0200 != 0 {
		t.Enchantments = new(CEnchantmentRegistry).Read(s)
	}
	if contentFlags&0x0008 != 0 {
		t.EventFilter = make([]Event, *s.readUint32()&0xFFFF)
		for i := range t.EventFilter {
			t.EventFilter[i] = Event(*s.readUint32())
		}
	}
	if contentFlags&0x0010 != 0 {
		t.EmoteTable = make([]EmoteTable, *s.readUint32()&0xFFFF)
		for i := range t.EmoteTable {
			t.EmoteTable[i] = *new(EmoteTable).Read(s)
		}
	}
	if contentFlags&0x0020 != 0 {
		t.CreateList = make([]CreationProfile, *s.readUint32()&0xFFFF)
		for i := range t.CreateList {
			t.CreateList[i] = *new(CreationProfile).Read(s)
		}
	}
	if contentFlags&0x0040 != 0 {
		t.PageDataList = new(PageDataList).Read(s)
	}
	if contentFlags&0x0080 != 0 {
		t.GeneratorTable = make([]Generator, *s.readUint32()&0xFFFF)
		for i := range t.GeneratorTable {
			t.GeneratorTable[i] = *new(Generator).Read(s)
		}
	}
	if contentFlags&0x0400 != 0 {
		t.GeneratorRegistry = make([]GeneratorRegistry, *s.readUint32()&0xFFFF)
		for i := range t.GeneratorRegistry {
			t.GeneratorRegistry[i] = *new(GeneratorRegistry).Read(s)
		}
	}
	if contentFlags&0x0800 != 0 {
		t.GeneratorQueue = make([]GeneratorQueue, *s.readUint32()&0xFFFF)
		for i := range t.GeneratorQueue {
			t.GeneratorQueue[i] = *new(GeneratorQueue).Read(s)
		}
	}
	return t
}

type Event uint32

type PageDataList struct {
	MaxNumPages        int32      `json:"maxNumPages"`
	MaxNumCharsPerPage int32      `json:"maxNumCharsPerPage"`
	Pages              []PageData `json:"pages"`
}

func (t *PageDataList) Read(s *SBuffer) *PageDataList {
	t.MaxNumPages = *s.readInt32()
	t.MaxNumCharsPerPage = *s.readInt32()
	t.Pages = make([]PageData, int(*s.readInt32()))
	for i := range t.Pages {
		t.Pages[i].Read(s)
	}
	return t
}

type PageData struct {
	AuthorID      uint32      `json:"authorID"`
	AuthorName    Dat_PString `json:"authorName"`
	AuthorAccount Dat_PString `json:"authorAccount"`
	PageText      Dat_PString `json:"pageText"`
}

func (t *PageData) Read(s *SBuffer) *PageData {
	t.AuthorID = *s.readUint32()
	t.AuthorName.Read(s, false)
	t.AuthorAccount.Read(s, false)
	t.PageText.Read(s, false)
	return t
}

type GeneratorQueue struct {
	Slot uint32  `json:"slot"`
	When float64 `json:"when"`
}

func (t *GeneratorQueue) Read(s *SBuffer) *GeneratorQueue {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type CEnchantmentRegistry struct {
	MultList     []Enchantment `json:"mult_list,omitempty"`
	AddList      []Enchantment `json:"add_list,omitempty"`
	CooldownList []Enchantment `json:"cooldown_list,omitempty"`
	Vitae        *Enchantment  `json:"vitae,omitempty"`
}

func (t *CEnchantmentRegistry) Read(s *SBuffer) *CEnchantmentRegistry {
	header := *s.readUint32()
	if header&1 != 0 {
		t.MultList = make([]Enchantment, int(*s.readUint32()&0xFFFF))
		for i := range t.MultList {
			t.MultList[i] = *new(Enchantment).Read(s)
		}
	}
	if header&2 != 0 {
		t.AddList = make([]Enchantment, int(*s.readUint32()&0xFFFF))
		for i := range t.AddList {
			t.AddList[i] = *new(Enchantment).Read(s)
		}
	}
	if header&8 != 0 {
		t.CooldownList = make([]Enchantment, int(*s.readUint32()&0xFFFF))
		for i := range t.CooldownList {
			t.CooldownList[i] = *new(Enchantment).Read(s)
		}
	}
	if header&4 != 0 {
		t.Vitae = new(Enchantment).Read(s)
	}
	return t
}

type Enchantment struct {
	Id               uint32  `json:"id"`
	SpellCategory    uint32  `json:"spell_category"`
	PowerLevel       int32   `json:"power_level"`
	StartTime        float64 `json:"start_time"`
	Duration         float64 `json:"duration"`
	Caster           uint32  `json:"caster"`
	DegradeModifier  float32 `json:"degrade_modifier"`
	DegradeLimit     float32 `json:"degrade_limit"`
	LastTimeDegraded float64 `json:"last_time_degraded"`
	SMod             StatMod `json:"smod"`
	SpellSetID       *uint32 `json:"spellset_id"` // present if SpellCategory&0xFFFF0000 != 0
}

func (t *Enchantment) Read(s *SBuffer) *Enchantment {
	t.Id = *s.readUint32()
	t.SpellCategory = *s.readUint32()
	t.PowerLevel = *s.readInt32()
	t.StartTime = *s.readDouble()
	t.Duration = *s.readDouble()
	t.Caster = *s.readUint32()
	t.DegradeModifier = *s.readSingle()
	t.DegradeLimit = *s.readSingle()
	t.LastTimeDegraded = *s.readDouble()
	t.SMod.Read(s)
	if t.SpellCategory&0xFFFF0000 != 0 {
		t.SpellSetID = s.readUint32()
	}
	return t
}

type GeneratorRegistry struct {
	WCID         uint32  `json:"wcidOrTtype"`
	TS           float64 `json:"ts"`
	TreasureType int32   `json:"bTreasureType"`
	Slot         uint32  `json:"slot"`
	Checkpointed int32   `json:"checkpointed"`
	Shop         int32   `json:"shop"`
	Amount       int32   `json:"amount"`
}

func (t *GeneratorRegistry) Read(s *SBuffer) *GeneratorRegistry {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type CBaseQualities struct {
	WeenieType    uint32         `json:"weenieType"`
	IntStats      []IntStat      `json:"intStats,omitempty"`    // STypeInt
	Int64Stats    []Int64Stat    `json:"int64Stats,omitempty"`  // STypeInt64
	BoolStats     []BoolStat     `json:"boolStats,omitempty"`   // STypeBool
	FloatStats    []FloatStat    `json:"floatStats,omitempty"`  // STypeFloat
	StringStats   []StringStat   `json:"stringStats,omitempty"` // STypeString
	DIDStats      []DIDStat      `json:"didStats,omitempty"`    // STypeDID
	IIDStats      []IIDStat      `json:"iidStats,omitempty"`    // STypeIID
	PositionStats []PositionStat `json:"posStats,omitempty"`    // STypePosition
}

func (t *CBaseQualities) Read(s *SBuffer) *CBaseQualities {
	contentFlags := *s.readUint32()
	t.WeenieType = *s.readUint32()
	if contentFlags&0x01 != 0 {
		t.IntStats = make([]IntStat, int(*s.readUint32()&0xFFFF))
		for i := range t.IntStats {
			key := *s.readUint32()
			t.IntStats[i] = IntStat{Key: key, Value: *s.readInt32()}
		}
	}
	if contentFlags&0x80 != 0 {
		t.Int64Stats = make([]Int64Stat, *s.readUint32()&0xFFFF)
		for i := range t.Int64Stats {
			key := *s.readUint32()
			t.Int64Stats[i] = Int64Stat{Key: key, Value: *s.readInt64()}
		}
	}
	if contentFlags&0x02 != 0 {
		t.BoolStats = make([]BoolStat, *s.readUint32()&0xFFFF)
		for i := range t.BoolStats {
			key := *s.readUint32()
			t.BoolStats[i] = BoolStat{Key: key, Value: byte(*s.readInt32())}
		}
	}
	if contentFlags&0x04 != 0 {
		t.FloatStats = make([]FloatStat, *s.readUint32()&0xFFFF)
		for i := range t.FloatStats {
			key := *s.readUint32()
			t.FloatStats[i] = FloatStat{Key: key, Value: *s.readDouble()}
		}
	}
	if contentFlags&0x10 != 0 {
		t.StringStats = make([]StringStat, *s.readUint32()&0xFFFF)
		for i := range t.StringStats {
			key := *s.readUint32()
			t.StringStats[i] = StringStat{Key: key, Value: *new(Dat_PString).Read(s, false)}
		}
	}
	if contentFlags&0x08 != 0 {
		t.DIDStats = make([]DIDStat, *s.readUint32()&0xFFFF)
		for i := range t.DIDStats {
			key := *s.readUint32()
			t.DIDStats[i] = DIDStat{Key: key, Value: *s.readUint32()}
		}
	}
	if contentFlags&0x40 != 0 {
		t.IIDStats = make([]IIDStat, *s.readUint32()&0xFFFF)
		for i := range t.IIDStats {
			key := *s.readUint32()
			t.IIDStats[i] = IIDStat{Key: key, Value: *s.readUint32()}
		}
	}
	if contentFlags&0x20 != 0 {
		t.PositionStats = make([]PositionStat, *s.readUint32()&0xFFFF)
		for i := range t.PositionStats {
			key := *s.readUint32()
			t.PositionStats[i] = PositionStat{Key: key, Value: *new(Position).Read(s)}
		}
	}
	return t
}
