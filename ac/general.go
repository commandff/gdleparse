package ac

import (
	"encoding/binary"
	"fmt"
	"strings"
)

type general struct {
	LastModified      *string      `json:"lastModified,omitempty"`
	ModifiedBy        *string      `json:"modifiedBy,omitempty"`
	Changelog         *[]changelog `json:"changelog,omitempty"`
	UserChangeSummary *string      `json:"userChangeSummary,omitempty"`
	IsDone            *bool        `json:"isDone,omitempty"`
}
type changelog struct {
	Created string `json:"created"`
	Author  string `json:"author"`
	Comment string `json:"comment"`
}

type Position struct {
	Objcell_id uint32 `json:"objcell_id"`
	Frame      Frame  `json:"frame"`
}

func (t *Position) Read(s *SBuffer) *Position {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (pos *Position) ToString() string {
	return fmt.Sprintf("0x%08X %s", pos.Objcell_id, pos.Frame.ToString())
}

type Frame struct {
	Origin Vector3    `json:"origin"`
	Angles Quaternion `json:"angles"`
}

func (t *Frame) Read(s *SBuffer) *Frame {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (f *Frame) ToString() string {
	return fmt.Sprintf("[%s] %s", f.Origin.ToString(), f.Angles.ToString())
}

type Quaternion struct {
	W float32 `json:"w"`
	Vector3
}

func (q *Quaternion) ToString() string {
	return fmt.Sprintf("%f %s", q.W, q.Vector3.ToString())
}

func (t *Quaternion) Read(s *SBuffer) *Quaternion {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Vector3 struct {
	X float32 `json:"x"`
	Y float32 `json:"y"`
	Z float32 `json:"z"`
}

func (v *Vector3) ToString() string {
	return fmt.Sprintf("%f %f %f", v.X, v.Y, v.Z)
}

func (t *Vector3) Read(s *SBuffer) *Vector3 {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type AttributeCache struct {
	Strength     *Attribute          `json:"strength"`
	Endurance    *Attribute          `json:"endurance"`
	Quickness    *Attribute          `json:"quickness"`
	Coordination *Attribute          `json:"coordination"`
	Focus        *Attribute          `json:"focus"`
	Self         *Attribute          `json:"self"`
	Health       *SecondaryAttribute `json:"health"`
	Stamina      *SecondaryAttribute `json:"stamina"`
	Mana         *SecondaryAttribute `json:"mana"`
}

func (t *AttributeCache) Read(s *SBuffer) *AttributeCache {
	contentFlags := *s.readUint32()
	if contentFlags&0x0001 != 0 {
		t.Strength = new(Attribute).Read(s)
	}
	if contentFlags&0x0002 != 0 {
		t.Endurance = new(Attribute).Read(s)
	}
	if contentFlags&0x0004 != 0 {
		t.Quickness = new(Attribute).Read(s)
	}
	if contentFlags&0x0008 != 0 {
		t.Coordination = new(Attribute).Read(s)
	}
	if contentFlags&0x0010 != 0 {
		t.Focus = new(Attribute).Read(s)
	}
	if contentFlags&0x0020 != 0 {
		t.Self = new(Attribute).Read(s)
	}
	if contentFlags&0x0040 != 0 {
		t.Health = new(SecondaryAttribute).Read(s)
	}
	if contentFlags&0x0080 != 0 {
		t.Stamina = new(SecondaryAttribute).Read(s)
	}
	if contentFlags&0x0100 != 0 {
		t.Mana = new(SecondaryAttribute).Read(s)
	}
	return t
}

type Attribute struct {
	LevelFromCp uint32 `json:"level_from_cp"`
	InitLevel   uint32 `json:"init_level"`
	CpSpent     uint32 `json:"cp_spent"`
}

func (t *Attribute) Read(s *SBuffer) *Attribute {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type SecondaryAttribute struct {
	Attribute
	Current uint32 `json:"current"`
}

func (t *SecondaryAttribute) Read(s *SBuffer) *SecondaryAttribute {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Skill struct {
	LevelFromPp           uint16  `json:"level_from_pp"`
	Padding               uint16  `json:"-"`
	Sac                   int32   `json:"sac"`
	Pp                    uint32  `json:"pp"`
	InitLevel             uint32  `json:"init_level"`
	ResistanceOfLastCheck int32   `json:"resistance_of_last_check"`
	LastTimeUsed          float64 `json:"last_used_time"`
}

func (t *Skill) Read(s *SBuffer) *Skill {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Body struct {
	BodyPartTable []BodyPartTable `json:"body_part_table"`
}

func (t *Body) Read(s *SBuffer) *Body {
	t.BodyPartTable = make([]BodyPartTable, *s.readUint32()&0xFFFF)
	for i := range t.BodyPartTable {
		key := *s.readUint32()
		t.BodyPartTable[i] = BodyPartTable{Key: key, Value: *new(BodyPart).Read(s)}
	}
	return t
}

type BodyPartTable struct {
	Key   uint32   `json:"key"`
	Value BodyPart `json:"value"`
}

type BodyPart struct {
	Dtype  Dtype                  `json:"dtype"`
	Dval   int32                  `json:"dval"`
	Dvar   float32                `json:"dvar"`
	Acache ArmorCache             `json:"acache"`
	Bh     BodyHeight             `json:"bh"`
	Bpsd   *BodyPartSelectionData `json:"bpsd"`
}

func (t *BodyPart) Read(s *SBuffer) *BodyPart {
	hasBPSD := *s.readInt32() == 1
	t.Dtype = Dtype(*s.readUint32())
	t.Dval = *s.readInt32()
	t.Dvar = *s.readSingle()
	t.Acache.Read(s)
	t.Bh = BodyHeight(*s.readUint32())
	if hasBPSD {
		t.Bpsd = new(BodyPartSelectionData).Read(s)
	}
	return t
}

type BodyHeight uint32

func (bh BodyHeight) ToString() string {
	switch bh {
	case 0:
		return "UNDEF"
	case 1:
		return "HIGH"
	case 2:
		return "MEDIUM"
	case 3:
		return "LOW"
	default:
		return "*ERR*"
	}
}

type Dtype int32

func (t Dtype) ToString() string {
	var reta []string = make([]string, 0)
	if t&1 != 0 {
		reta = append(reta, "SLASH")
	}
	if t&2 != 0 {
		reta = append(reta, "PIERCE")
	}
	if t&4 != 0 {
		reta = append(reta, "BLUDGEON")
	}
	if t&8 != 0 {
		reta = append(reta, "COLD")
	}
	if t&0x10 != 0 {
		reta = append(reta, "FIRE")
	}
	if t&0x20 != 0 {
		reta = append(reta, "ACID")
	}
	if t&0x40 != 0 {
		reta = append(reta, "ELECTRIC")
	}
	if t&0x80 != 0 {
		reta = append(reta, "HEALTH")
	}
	if t&0x100 != 0 {
		reta = append(reta, "STAMINA")
	}
	if t&0x200 != 0 {
		reta = append(reta, "MANA")
	}
	if t&0x400 != 0 {
		reta = append(reta, "NETHER")
	}
	if t&0x10000000 != 0 {
		reta = append(reta, "BASE")
	}
	if len(reta) == 0 {
		return "UNDEF"
	}
	return strings.Join(reta, "/")
}

type ArmorCache struct {
	BaseArmor       int32 `json:"base_armor"`
	ArmorVsSlash    int32 `json:"armor_vs_slash"`
	ArmorVsPierce   int32 `json:"armor_vs_pierce"`
	ArmorVsBludgeon int32 `json:"armor_vs_bludgeon"`
	ArmorVsCold     int32 `json:"armor_vs_cold"`
	ArmorVsFire     int32 `json:"armor_vs_fire"`
	ArmorVsAcid     int32 `json:"armor_vs_acid"`
	ArmorVsElectric int32 `json:"armor_vs_electric"`
	ArmorVsNether   int32 `json:"armor_vs_nether"`
}

func (t *ArmorCache) Read(s *SBuffer) *ArmorCache {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func fOrZero(baseArmor int32, armorVs int32) float64 {
	if baseArmor == 0 || armorVs == 0 {
		return 0.0
	}
	return float64(baseArmor) / float64(armorVs)
}
func (a *ArmorCache) ToString() string {
	return fmt.Sprintf("AL:%d,S:%f,P:%f,B:%f,C:%f,F:%f,A:%f,E:%f,N:%f", a.BaseArmor,
		fOrZero(a.BaseArmor, a.ArmorVsSlash),
		fOrZero(a.BaseArmor, a.ArmorVsPierce),
		fOrZero(a.BaseArmor, a.ArmorVsBludgeon),
		fOrZero(a.BaseArmor, a.ArmorVsCold),
		fOrZero(a.BaseArmor, a.ArmorVsFire),
		fOrZero(a.BaseArmor, a.ArmorVsAcid),
		fOrZero(a.BaseArmor, a.ArmorVsElectric),
		fOrZero(a.BaseArmor, a.ArmorVsNether),
	)
}

type BodyPartSelectionData struct {
	Hlf float32 `json:"HLF"`
	Mlf float32 `json:"MLF"`
	Llf float32 `json:"LLF"`
	Hrf float32 `json:"HRF"`
	Mrf float32 `json:"MRF"`
	Lrf float32 `json:"LRF"`
	Hlb float32 `json:"HLB"`
	Mlb float32 `json:"MLB"`
	Llb float32 `json:"LLB"`
	Hrb float32 `json:"HRB"`
	Mrb float32 `json:"MRB"`
	Lrb float32 `json:"LRB"`
}

func (t *BodyPartSelectionData) Read(s *SBuffer) *BodyPartSelectionData {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (b *BodyPartSelectionData) ToString() string {
	return fmt.Sprintf("Hlf:%f,Mlf:%f,Llf:%f,Hrf:%f,Mrf:%f,Lrf:%f,Hlb:%f,Mlb:%f,Llb:%f,Hrb:%f,Mrb:%f,Lrb:%f",
		b.Hlf, b.Mlf, b.Llf,
		b.Hrf, b.Mrf, b.Lrf,
		b.Hlb, b.Mlb, b.Llb,
		b.Hrb, b.Mrb, b.Lrb,
	)
}

type BoolStat struct {
	Key   uint32 `json:"key"`
	Value byte   `json:"value"`
}

var STypeBool []string = []string{"UNDEF", "STUCK", "OPEN", "LOCKED", "ROT_PROOF", "ALLEGIANCE_UPDATE_REQUEST", "AI_USES_MANA", "AI_USE_HUMAN_MAGIC_ANIMATIONS", "ALLOW_GIVE", "CURRENTLY_ATTACKING", "ATTACKER_AI", "IGNORE_COLLISIONS", "REPORT_COLLISIONS", "ETHEREAL", "GRAVITY_STATUS", "LIGHTS_STATUS", "SCRIPTED_COLLISION", "INELASTIC", "VISIBILITY", "ATTACKABLE", "SAFE_SPELL_COMPONENTS", "ADVOCATE_STATE", "INSCRIBABLE", "DESTROY_ON_SELL", "UI_HIDDEN", "IGNORE_HOUSE_BARRIERS", "HIDDEN_ADMIN", "PK_WOUNDER", "PK_KILLER", "NO_CORPSE", "UNDER_LIFESTONE_PROTECTION", "ITEM_MANA_UPDATE_PENDING", "GENERATOR_STATUS", "RESET_MESSAGE_PENDING", "DEFAULT_OPEN", "DEFAULT_LOCKED", "DEFAULT_ON", "OPEN_FOR_BUSINESS", "IS_FROZEN", "DEAL_MAGICAL_ITEMS", "LOGOFF_IM_DEAD", "REPORT_COLLISIONS_AS_ENVIRONMENT", "ALLOW_EDGE_SLIDE", "ADVOCATE_QUEST", "IS_ADMIN", "IS_ARCH", "IS_SENTINEL", "IS_ADVOCATE", "CURRENTLY_POWERING_UP", "GENERATOR_ENTERED_WORLD", "NEVER_FAIL_CASTING", "VENDOR_SERVICE", "AI_IMMOBILE", "DAMAGED_BY_COLLISIONS", "IS_DYNAMIC", "IS_HOT", "IS_AFFECTING", "AFFECTS_AIS", "SPELL_QUEUE_ACTIVE", "GENERATOR_DISABLED", "IS_ACCEPTING_TELLS", "LOGGING_CHANNEL", "OPENS_ANY_LOCK", "UNLIMITED_USE", "GENERATED_TREASURE_ITEM", "IGNORE_MAGIC_RESIST", "IGNORE_MAGIC_ARMOR", "AI_ALLOW_TRADE", "SPELL_COMPONENTS_REQUIRED", "IS_SELLABLE", "IGNORE_SHIELDS_BY_SKILL", "NODRAW", "ACTIVATION_UNTARGETED", "HOUSE_HAS_GOTTEN_PRIORITY_BOOT_POS", "GENERATOR_AUTOMATIC_DESTRUCTION", "HOUSE_HOOKS_VISIBLE", "HOUSE_REQUIRES_MONARCH", "HOUSE_HOOKS_ENABLED", "HOUSE_NOTIFIED_HUD_OF_HOOK_COUNT", "AI_ACCEPT_EVERYTHING", "IGNORE_PORTAL_RESTRICTIONS", "REQUIRES_BACKPACK_SLOT", "DONT_TURN_OR_MOVE_WHEN_GIVING", "NPC_LOOKS_LIKE_OBJECT", "IGNORE_CLO_ICONS", "APPRAISAL_HAS_ALLOWED_WIELDER", "CHEST_REGEN_ON_CLOSE", "LOGOFF_IN_MINIGAME", "PORTAL_SHOW_DESTINATION", "PORTAL_IGNORES_PK_ATTACK_TIMER", "NPC_INTERACTS_SILENTLY", "RETAINED", "IGNORE_AUTHOR", "LIMBO", "APPRAISAL_HAS_ALLOWED_ACTIVATOR", "EXISTED_BEFORE_ALLEGIANCE_XP_CHANGES", "IS_DEAF", "IS_PSR", "INVINCIBLE", "IVORYABLE", "DYABLE", "CAN_GENERATE_RARE", "CORPSE_GENERATED_RARE", "NON_PROJECTILE_MAGIC_IMMUNE", "ACTD_RECEIVED_ITEMS", "EXECUTING_EMOTE", "FIRST_ENTER_WORLD_DONE", "RECALLS_DISABLED", "RARE_USES_TIMER", "ACTD_PREORDER_RECEIVED_ITEMS", "AFK", "IS_GAGGED", "PROC_SPELL_SELF_TARGETED", "IS_ALLEGIANCE_GAGGED", "EQUIPMENT_SET_TRIGGER_PIECE", "UNINSCRIBE", "WIELD_ON_USE", "CHEST_CLEARED_WHEN_CLOSED", "NEVER_ATTACK", "SUPPRESS_GENERATE_EFFECT", "TREASURE_CORPSE", "EQUIPMENT_SET_ADD_LEVEL", "BARBER_ACTIVE", "TOP_LAYER_PRIORITY", "NO_HELD_ITEM_SHOWN", "LOGIN_AT_LIFESTONE", "OLTHOI_PK", "ACCOUNT_15_DAYS", "HAD_NO_VITAE", "NO_OLTHOI_TALK", "AUTOWIELD_LEFT", "MERGE_LOCKED"}

func (stat *BoolStat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypeBool)) {
		stat_name = STypeBool[stat.Key]
	}
	return fmt.Sprintf("%s (%d): %T", stat_name, stat.Key, stat.Value == 1)
}

type IntStat struct {
	Key   uint32 `json:"key"`
	Value int32  `json:"value"`
}

var STypeInt []string = []string{"UNDEF", "ITEM_TYPE", "CREATURE_TYPE", "PALETTE_TEMPLATE", "CLOTHING_PRIORITY", "ENCUMB_VAL", "ITEMS_CAPACITY", "CONTAINERS_CAPACITY", "MASS", "LOCATIONS", "CURRENT_WIELDED_LOCATION", "MAX_STACK_SIZE", "STACK_SIZE", "STACK_UNIT_ENCUMB", "STACK_UNIT_MASS", "STACK_UNIT_VALUE", "ITEM_USEABLE", "RARE_ID", "UI_EFFECTS", "VALUE", "COIN_VALUE", "TOTAL_EXPERIENCE", "AVAILABLE_CHARACTER", "TOTAL_SKILL_CREDITS", "AVAILABLE_SKILL_CREDITS", "LEVEL", "ACCOUNT_REQUIREMENTS", "ARMOR_TYPE", "ARMOR_LEVEL", "ALLEGIANCE_CP_POOL", "ALLEGIANCE_RANK", "CHANNELS_ALLOWED", "CHANNELS_ACTIVE", "BONDED", "MONARCHS_RANK", "ALLEGIANCE_FOLLOWERS", "RESIST_MAGIC", "RESIST_ITEM_APPRAISAL", "RESIST_LOCKPICK", "DEPRECATED_RESIST_REPAIR", "COMBAT_MODE", "CURRENT_ATTACK_HEIGHT", "COMBAT_COLLISIONS", "NUM_DEATHS", "DAMAGE", "DAMAGE_TYPE", "DEFAULT_COMBAT_STYLE", "ATTACK_TYPE", "WEAPON_SKILL", "WEAPON_TIME", "AMMO_TYPE", "COMBAT_USE", "PARENT_LOCATION", "PLACEMENT_POSITION", "WEAPON_ENCUMBRANCE", "WEAPON_MASS", "SHIELD_VALUE", "SHIELD_ENCUMBRANCE", "MISSILE_INVENTORY_LOCATION", "FULL_DAMAGE_TYPE", "WEAPON_RANGE", "ATTACKERS_SKILL", "DEFENDERS_SKILL", "ATTACKERS_SKILL_VALUE", "ATTACKERS_CLASS", "PLACEMENT", "CHECKPOINT_STATUS", "TOLERANCE", "TARGETING_TACTIC", "COMBAT_TACTIC", "HOMESICK_TARGETING_TACTIC", "NUM_FOLLOW_FAILURES", "FRIEND_TYPE", "FOE_TYPE", "MERCHANDISE_ITEM_TYPES", "MERCHANDISE_MIN_VALUE", "MERCHANDISE_MAX_VALUE", "NUM_ITEMS_SOLD", "NUM_ITEMS_BOUGHT", "MONEY_INCOME", "MONEY_OUTFLOW", "MAX_GENERATED_OBJECTS", "INIT_GENERATED_OBJECTS", "ACTIVATION_RESPONSE", "ORIGINAL_VALUE", "NUM_MOVE_FAILURES", "MIN_LEVEL", "MAX_LEVEL", "LOCKPICK_MOD", "BOOSTER_ENUM", "BOOST_VALUE", "MAX_STRUCTURE", "STRUCTURE", "PHYSICS_STATE", "TARGET_TYPE", "RADARBLIP_COLOR", "ENCUMB_CAPACITY", "LOGIN_TIMESTAMP", "CREATION_TIMESTAMP", "PK_LEVEL_MODIFIER", "GENERATOR_TYPE", "AI_ALLOWED_COMBAT_STYLE", "LOGOFF_TIMESTAMP", "GENERATOR_DESTRUCTION_TYPE", "ACTIVATION_CREATE_CLASS", "ITEM_WORKMANSHIP", "ITEM_SPELLCRAFT", "ITEM_CUR_MANA", "ITEM_MAX_MANA", "ITEM_DIFFICULTY", "ITEM_ALLEGIANCE_RANK_LIMIT", "PORTAL_BITMASK", "ADVOCATE_LEVEL", "GENDER", "ATTUNED", "ITEM_SKILL_LEVEL_LIMIT", "GATE_LOGIC", "ITEM_MANA_COST", "LOGOFF", "ACTIVE", "ATTACK_HEIGHT", "NUM_ATTACK_FAILURES", "AI_CP_THRESHOLD", "AI_ADVANCEMENT_STRATEGY", "VERSION", "AGE", "VENDOR_HAPPY_MEAN", "VENDOR_HAPPY_VARIANCE", "CLOAK_STATUS", "VITAE_CP_POOL", "NUM_SERVICES_SOLD", "MATERIAL_TYPE", "NUM_ALLEGIANCE_BREAKS", "SHOWABLE_ON_RADAR", "PLAYER_KILLER_STATUS", "VENDOR_HAPPY_MAX_ITEMS", "SCORE_PAGE_NUM", "SCORE_CONFIG_NUM", "SCORE_NUM_SCORES", "DEATH_LEVEL", "AI_OPTIONS", "OPEN_TO_EVERYONE", "GENERATOR_TIME_TYPE", "GENERATOR_START_TIME", "GENERATOR_END_TIME", "GENERATOR_END_DESTRUCTION_TYPE",
	"XP_OVERRIDE", "NUM_CRASH_AND_TURNS", "COMPONENT_WARNING_THRESHOLD", "HOUSE_STATUS", "HOOK_PLACEMENT", "HOOK_TYPE", "HOOK_ITEM_TYPE", "AI_PP_THRESHOLD", "GENERATOR_VERSION", "HOUSE_TYPE", "PICKUP_EMOTE_OFFSET", "WEENIE_ITERATION", "WIELD_REQUIREMENTS", "WIELD_SKILLTYPE", "WIELD_DIFFICULTY", "HOUSE_MAX_HOOKS_USABLE", "HOUSE_CURRENT_HOOKS_USABLE", "ALLEGIANCE_MIN_LEVEL", "ALLEGIANCE_MAX_LEVEL", "HOUSE_RELINK_HOOK_COUNT", "SLAYER_CREATURE_TYPE", "CONFIRMATION_IN_PROGRESS", "CONFIRMATION_TYPE_IN_PROGRESS", "TSYS_MUTATION_DATA", "NUM_ITEMS_IN_MATERIAL", "NUM_TIMES_TINKERED", "APPRAISAL_LONG_DESC_DECORATION", "APPRAISAL_LOCKPICK_SUCCESS_PERCENT", "APPRAISAL_PAGES", "APPRAISAL_MAX_PAGES", "APPRAISAL_ITEM_SKILL", "GEM_COUNT", "GEM_TYPE", "IMBUED_EFFECT", "ATTACKERS_RAW_SKILL_VALUE", "CHESS_RANK", "CHESS_TOTALGAMES", "CHESS_GAMESWON", "CHESS_GAMESLOST", "TYPE_OF_ALTERATION", "SKILL_TO_BE_ALTERED", "SKILL_ALTERATION_COUNT", "HERITAGE_GROUP", "TRANSFER_FROM_ATTRIBUTE", "TRANSFER_TO_ATTRIBUTE", "ATTRIBUTE_TRANSFER_COUNT", "FAKE_FISHING_SKILL", "NUM_KEYS", "DEATH_TIMESTAMP", "PK_TIMESTAMP", "VICTIM_TIMESTAMP", "HOOK_GROUP", "ALLEGIANCE_SWEAR_TIMESTAMP", "HOUSE_PURCHASE_TIMESTAMP", "REDIRECTABLE_EQUIPPED_ARMOR_COUNT", "MELEEDEFENSE_IMBUEDEFFECTTYPE_CACHE", "MISSILEDEFENSE_IMBUEDEFFECTTYPE_CACHE", "MAGICDEFENSE_IMBUEDEFFECTTYPE_CACHE", "ELEMENTAL_DAMAGE_BONUS", "IMBUE_ATTEMPTS", "IMBUE_SUCCESSES", "CREATURE_KILLS", "PLAYER_KILLS_PK", "PLAYER_KILLS_PKL", "RARES_TIER_ONE", "RARES_TIER_TWO", "RARES_TIER_THREE", "RARES_TIER_FOUR", "RARES_TIER_FIVE", "AUGMENTATION_STAT", "AUGMENTATION_FAMILY_STAT", "AUGMENTATION_INNATE_FAMILY", "AUGMENTATION_INNATE_STRENGTH", "AUGMENTATION_INNATE_ENDURANCE", "AUGMENTATION_INNATE_COORDINATION", "AUGMENTATION_INNATE_QUICKNESS", "AUGMENTATION_INNATE_FOCUS", "AUGMENTATION_INNATE_SELF", "AUGMENTATION_SPECIALIZE_SALVAGING", "AUGMENTATION_SPECIALIZE_ITEM_TINKERING", "AUGMENTATION_SPECIALIZE_ARMOR_TINKERING", "AUGMENTATION_SPECIALIZE_MAGIC_ITEM_TINKERING", "AUGMENTATION_SPECIALIZE_WEAPON_TINKERING", "AUGMENTATION_EXTRA_PACK_SLOT", "AUGMENTATION_INCREASED_CARRYING_CAPACITY", "AUGMENTATION_LESS_DEATH_ITEM_LOSS", "AUGMENTATION_SPELLS_REMAIN_PAST_DEATH", "AUGMENTATION_CRITICAL_DEFENSE", "AUGMENTATION_BONUS_XP", "AUGMENTATION_BONUS_SALVAGE", "AUGMENTATION_BONUS_IMBUE_CHANCE", "AUGMENTATION_FASTER_REGEN", "AUGMENTATION_INCREASED_SPELL_DURATION", "AUGMENTATION_RESISTANCE_FAMILY", "AUGMENTATION_RESISTANCE_SLASH", "AUGMENTATION_RESISTANCE_PIERCE", "AUGMENTATION_RESISTANCE_BLUNT", "AUGMENTATION_RESISTANCE_ACID", "AUGMENTATION_RESISTANCE_FIRE", "AUGMENTATION_RESISTANCE_FROST", "AUGMENTATION_RESISTANCE_LIGHTNING", "RARES_TIER_ONE_LOGIN", "RARES_TIER_TWO_LOGIN", "RARES_TIER_THREE_LOGIN", "RARES_TIER_FOUR_LOGIN", "RARES_TIER_FIVE_LOGIN", "RARES_LOGIN_TIMESTAMP", "RARES_TIER_SIX", "RARES_TIER_SEVEN", "RARES_TIER_SIX_LOGIN", "RARES_TIER_SEVEN_LOGIN", "ITEM_ATTRIBUTE_LIMIT",
	"ITEM_ATTRIBUTE_LEVEL_LIMIT", "ITEM_ATTRIBUTE_2ND_LIMIT", "ITEM_ATTRIBUTE_2ND_LEVEL_LIMIT", "CHARACTER_TITLE_ID", "NUM_CHARACTER_TITLES", "RESISTANCE_MODIFIER_TYPE", "FREE_TINKERS_BITFIELD", "EQUIPMENT_SET_ID", "PET_CLASS", "LIFESPAN", "REMAINING_LIFESPAN", "USE_CREATE_QUANTITY", "WIELD_REQUIREMENTS_2", "WIELD_SKILLTYPE_2", "WIELD_DIFFICULTY_2", "WIELD_REQUIREMENTS_3", "WIELD_SKILLTYPE_3", "WIELD_DIFFICULTY_3", "WIELD_REQUIREMENTS_4", "WIELD_SKILLTYPE_4", "WIELD_DIFFICULTY_4", "UNIQUE", "SHARED_COOLDOWN", "FACTION1_BITS", "FACTION2_BITS", "FACTION3_BITS", "HATRED1_BITS", "HATRED2_BITS", "HATRED3_BITS", "SOCIETY_RANK_CELHAN", "SOCIETY_RANK_ELDWEB", "SOCIETY_RANK_RADBLO", "HEAR_LOCAL_SIGNALS", "HEAR_LOCAL_SIGNALS_RADIUS", "CLEAVING", "AUGMENTATION_SPECIALIZE_GEARCRAFT", "AUGMENTATION_INFUSED_CREATURE_MAGIC", "AUGMENTATION_INFUSED_ITEM_MAGIC", "AUGMENTATION_INFUSED_LIFE_MAGIC", "AUGMENTATION_INFUSED_WAR_MAGIC", "AUGMENTATION_CRITICAL_EXPERTISE", "AUGMENTATION_CRITICAL_POWER", "AUGMENTATION_SKILLED_MELEE", "AUGMENTATION_SKILLED_MISSILE", "AUGMENTATION_SKILLED_MAGIC", "IMBUED_EFFECT_2", "IMBUED_EFFECT_3", "IMBUED_EFFECT_4", "IMBUED_EFFECT_5", "DAMAGE_RATING", "DAMAGE_RESIST_RATING", "AUGMENTATION_DAMAGE_BONUS", "AUGMENTATION_DAMAGE_REDUCTION", "IMBUE_STACKING_BITS", "HEAL_OVER_TIME", "CRIT_RATING", "CRIT_DAMAGE_RATING", "CRIT_RESIST_RATING", "CRIT_DAMAGE_RESIST_RATING", "HEALING_RESIST_RATING", "DAMAGE_OVER_TIME", "ITEM_MAX_LEVEL", "ITEM_XP_STYLE", "EQUIPMENT_SET_EXTRA", "AETHERIA_BITFIELD", "HEALING_BOOST_RATING", "HERITAGE_SPECIFIC_ARMOR", "ALTERNATE_RACIAL_SKILLS", "AUGMENTATION_JACK_OF_ALL_TRADES", "AUGMENTATION_RESISTANCE_NETHER", "AUGMENTATION_INFUSED_VOID_MAGIC", "WEAKNESS_RATING", "NETHER_OVER_TIME", "NETHER_RESIST_RATING", "LUMINANCE_AWARD", "LUM_AUG_DAMAGE_RATING", "LUM_AUG_DAMAGE_REDUCTION_RATING", "LUM_AUG_CRIT_DAMAGE_RATING", "LUM_AUG_CRIT_REDUCTION_RATING", "LUM_AUG_SURGE_EFFECT_RATING", "LUM_AUG_SURGE_CHANCE_RATING", "LUM_AUG_ITEM_MANA_USAGE", "LUM_AUG_ITEM_MANA_GAIN", "LUM_AUG_VITALITY", "LUM_AUG_HEALING_RATING", "LUM_AUG_SKILLED_CRAFT", "LUM_AUG_SKILLED_SPEC", "LUM_AUG_NO_DESTROY_CRAFT", "RESTRICT_INTERACTION", "OLTHOI_LOOT_TIMESTAMP", "OLTHOI_LOOT_STEP", "USE_CREATES_CONTRACT_ID", "DOT_RESIST_RATING", "LIFE_RESIST_RATING", "CLOAK_WEAVE_PROC", "WEAPON_TYPE", "MELEE_MASTERY", "RANGED_MASTERY", "SNEAK_ATTACK_RATING", "RECKLESSNESS_RATING", "DECEPTION_RATING", "COMBAT_PET_RANGE", "WEAPON_AURA_DAMAGE", "WEAPON_AURA_SPEED", "SUMMONING_MASTERY", "HEARTBEAT_LIFESPAN", "USE_LEVEL_REQUIREMENT", "LUM_AUG_ALL_SKILLS", "USE_REQUIRES_SKILL", "USE_REQUIRES_SKILL_LEVEL", "USE_REQUIRES_SKILL_SPEC", "USE_REQUIRES_LEVEL", "GEAR_DAMAGE", "GEAR_DAMAGE_RESIST", "GEAR_CRIT", "GEAR_CRIT_RESIST", "GEAR_CRIT_DAMAGE", "GEAR_CRIT_DAMAGE_RESIST", "GEAR_HEALING_BOOST", "GEAR_NETHER_RESIST", "GEAR_LIFE_RESIST", "GEAR_MAX_HEALTH", "UNKNOWN_380",
	"PK_DAMAGE_RATING", "PK_DAMAGE_RESIST_RATING", "GEAR_PK_DAMAGE_RATING", "GEAR_PK_DAMAGE_RESIST_RATING", "UNKNOWN_385_SEEN", "OVERPOWER", "OVERPOWER_RESIST", "GEAR_OVERPOWER", "GEAR_OVERPOWER_RESIST", "ENLIGHTENMENT"}

type DIDStat struct {
	Key   uint32 `json:"key"`
	Value uint32 `json:"value"`
}

var STypeDID []string = []string{"UNDEF", "SETUP", "MOTION_TABLE", "SOUND_TABLE", "COMBAT_TABLE", "QUALITY_FILTER", "PALETTE_BASE", "CLOTHINGBASE", "ICON", "EYES_TEXTURE", "NOSE_TEXTURE", "MOUTH_TEXTURE", "DEFAULT_EYES_TEXTURE", "DEFAULT_NOSE_TEXTURE", "DEFAULT_MOUTH_TEXTURE", "HAIR_PALETTE", "EYES_PALETTE", "SKIN_PALETTE", "HEAD_OBJECT", "ACTIVATION_ANIMATION", "INIT_MOTION", "ACTIVATION_SOUND", "PHYSICS_EFFECT_TABLE", "USE_SOUND", "USE_TARGET_ANIMATION", "USE_TARGET_SUCCESS_ANIMATION", "USE_TARGET_FAILURE_ANIMATION", "USE_USER_ANIMATION", "SPELL", "SPELL_COMPONENT", "PHYSICS_SCRIPT", "LINKED_PORTAL_ONE", "WIELDED_TREASURE_TYPE", "INVENTORY_TREASURE_TYPE", "SHOP_TREASURE_TYPE", "DEATH_TREASURE_TYPE", "MUTATE_FILTER", "ITEM_SKILL_LIMIT", "USE_CREATE_ITEM", "DEATH_SPELL", "VENDORS_CLASSID", "ITEM_SPECIALIZED_ONLY", "HOUSEID", "ACCOUNT_HOUSEID", "RESTRICTION_EFFECT", "CREATION_MUTATION_FILTER", "TSYS_MUTATION_FILTER", "LAST_PORTAL", "LINKED_PORTAL_TWO", "ORIGINAL_PORTAL", "ICON_OVERLAY", "ICON_OVERLAY_SECONDARY", "ICON_UNDERLAY", "AUGMENTATION_MUTATION_FILTER", "AUGMENTATION_EFFECT", "PROC_SPELL", "AUGMENTATION_CREATE_ITEM", "ALTERNATE_CURRENCY", "BLUE_SURGE_SPELL", "YELLOW_SURGE_SPELL", "RED_SURGE_SPELL", "OLTHOI_DEATH_TREASURE_TYPE"}

func (stat *DIDStat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypeDID)) {
		stat_name = STypeDID[stat.Key]
	}
	return fmt.Sprintf("%s (%d): 0x%08X (%d)", stat_name, stat.Key, stat.Value, stat.Value)
}

type IIDStat struct {
	Key   uint32 `json:"key"`
	Value uint32 `json:"value"`
}

var STypeIID []string = []string{"UNDEF", "OWNER", "CONTAINER", "WIELDER", "FREEZER", "VIEWER", "GENERATOR", "SCRIBE", "CURRENT_COMBAT_TARGET", "CURRENT_ENEMY", "PROJECTILE_LAUNCHER", "CURRENT_ATTACKER", "CURRENT_DAMAGER", "CURRENT_FOLLOW_TARGET", "CURRENT_APPRAISAL_TARGET", "CURRENT_FELLOWSHIP_APPRAISAL_TARGET", "ACTIVATION_TARGET", "CREATOR", "VICTIM", "KILLER", "VENDOR", "CUSTOMER", "BONDED", "WOUNDER", "ALLEGIANCE", "PATRON", "MONARCH", "COMBAT_TARGET", "HEALTH_QUERY_TARGET", "LAST_UNLOCKER", "CRASH_AND_TURN_TARGET", "ALLOWED_ACTIVATOR", "HOUSE_OWNER", "HOUSE", "SLUMLORD", "MANA_QUERY_TARGET", "CURRENT_GAME", "REQUESTED_APPRAISAL_TARGET", "ALLOWED_WIELDER", "ASSIGNED_TARGET", "LIMBO_SOURCE", "SNOOPER", "TELEPORTED_CHARACTER", "PET", "PET_OWNER", "PET_DEVICE"}

func (stat *IIDStat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypeIID)) {
		stat_name = STypeIID[stat.Key]
	}
	return fmt.Sprintf("%s (%d): 0x%08X (%d)", stat_name, stat.Key, stat.Value, stat.Value)
}

type FloatStat struct {
	Key   uint32  `json:"key"`
	Value float64 `json:"value"`
}

var STypeFloat []string = []string{"UNDEF", "HEARTBEAT_INTERVAL", "HEARTBEAT_TIMESTAMP", "HEALTH_RATE", "STAMINA_RATE", "MANA_RATE", "HEALTH_UPON_RESURRECTION", "STAMINA_UPON_RESURRECTION", "MANA_UPON_RESURRECTION", "START_TIME", "STOP_TIME", "RESET_INTERVAL", "SHADE", "ARMOR_MOD_VS_SLASH", "ARMOR_MOD_VS_PIERCE", "ARMOR_MOD_VS_BLUDGEON", "ARMOR_MOD_VS_COLD", "ARMOR_MOD_VS_FIRE", "ARMOR_MOD_VS_ACID", "ARMOR_MOD_VS_ELECTRIC", "COMBAT_SPEED", "WEAPON_LENGTH", "DAMAGE_VARIANCE", "CURRENT_POWER_MOD", "ACCURACY_MOD", "STRENGTH_MOD", "MAXIMUM_VELOCITY", "ROTATION_SPEED", "MOTION_TIMESTAMP", "WEAPON_DEFENSE", "WIMPY_LEVEL", "VISUAL_AWARENESS_RANGE", "AURAL_AWARENESS_RANGE", "PERCEPTION_LEVEL", "POWERUP_TIME", "MAX_CHARGE_DISTANCE", "CHARGE_SPEED", "BUY_PRICE", "SELL_PRICE", "DEFAULT_SCALE", "LOCKPICK_MOD", "REGENERATION_INTERVAL", "REGENERATION_TIMESTAMP", "GENERATOR_RADIUS", "TIME_TO_ROT", "DEATH_TIMESTAMP", "PK_TIMESTAMP", "VICTIM_TIMESTAMP", "LOGIN_TIMESTAMP", "CREATION_TIMESTAMP", "MINIMUM_TIME_SINCE_PK", "DEPRECATED_HOUSEKEEPING_PRIORITY", "ABUSE_LOGGING_TIMESTAMP", "LAST_PORTAL_TELEPORT_TIMESTAMP", "USE_RADIUS", "HOME_RADIUS", "RELEASED_TIMESTAMP", "MIN_HOME_RADIUS", "FACING", "RESET_TIMESTAMP", "LOGOFF_TIMESTAMP", "ECON_RECOVERY_INTERVAL", "WEAPON_OFFENSE", "DAMAGE_MOD", "RESIST_SLASH", "RESIST_PIERCE", "RESIST_BLUDGEON", "RESIST_FIRE", "RESIST_COLD", "RESIST_ACID", "RESIST_ELECTRIC", "RESIST_HEALTH_BOOST", "RESIST_STAMINA_DRAIN", "RESIST_STAMINA_BOOST", "RESIST_MANA_DRAIN", "RESIST_MANA_BOOST", "TRANSLUCENCY", "PHYSICS_SCRIPT_INTENSITY", "FRICTION", "ELASTICITY", "AI_USE_MAGIC_DELAY", "ITEM_MIN_SPELLCRAFT_MOD", "ITEM_MAX_SPELLCRAFT_MOD", "ITEM_RANK_PROBABILITY", "SHADE2", "SHADE3", "SHADE4", "ITEM_EFFICIENCY", "ITEM_MANA_UPDATE_TIMESTAMP", "SPELL_GESTURE_SPEED_MOD", "SPELL_STANCE_SPEED_MOD", "ALLEGIANCE_APPRAISAL_TIMESTAMP", "POWER_LEVEL", "ACCURACY_LEVEL", "ATTACK_ANGLE", "ATTACK_TIMESTAMP", "CHECKPOINT_TIMESTAMP", "SOLD_TIMESTAMP", "USE_TIMESTAMP", "USE_LOCK_TIMESTAMP", "HEALKIT_MOD", "FROZEN_TIMESTAMP", "HEALTH_RATE_MOD", "ALLEGIANCE_SWEAR_TIMESTAMP", "OBVIOUS_RADAR_RANGE", "HOTSPOT_CYCLE_TIME", "HOTSPOT_CYCLE_TIME_VARIANCE", "SPAM_TIMESTAMP", "SPAM_RATE", "BOND_WIELDED_TREASURE", "BULK_MOD", "SIZE_MOD", "GAG_TIMESTAMP", "GENERATOR_UPDATE_TIMESTAMP", "DEATH_SPAM_TIMESTAMP", "DEATH_SPAM_RATE", "WILD_ATTACK_PROBABILITY", "FOCUSED_PROBABILITY", "CRASH_AND_TURN_PROBABILITY", "CRASH_AND_TURN_RADIUS", "CRASH_AND_TURN_BIAS", "GENERATOR_INITIAL_DELAY", "AI_ACQUIRE_HEALTH", "AI_ACQUIRE_STAMINA", "AI_ACQUIRE_MANA", "RESIST_HEALTH_DRAIN", "LIFESTONE_PROTECTION_TIMESTAMP", "AI_COUNTERACT_ENCHANTMENT", "AI_DISPEL_ENCHANTMENT", "TRADE_TIMESTAMP", "AI_TARGETED_DETECTION_RADIUS", "EMOTE_PRIORITY", "LAST_TELEPORT_START_TIMESTAMP", "EVENT_SPAM_TIMESTAMP", "EVENT_SPAM_RATE", "INVENTORY_OFFSET", "CRITICAL_MULTIPLIER", "MANA_STONE_DESTROY_CHANCE", "SLAYER_DAMAGE_BONUS", "ALLEGIANCE_INFO_SPAM_TIMESTAMP", "ALLEGIANCE_INFO_SPAM_RATE", "NEXT_SPELLCAST_TIMESTAMP", "APPRAISAL_REQUESTED_TIMESTAMP", "APPRAISAL_HEARTBEAT_DUE_TIMESTAMP", "MANA_CONVERSION_MOD", "LAST_PK_ATTACK_TIMESTAMP", "FELLOWSHIP_UPDATE_TIMESTAMP", "CRITICAL_FREQUENCY", "LIMBO_START_TIMESTAMP", "WEAPON_MISSILE_DEFENSE", "WEAPON_MAGIC_DEFENSE", "IGNORE_SHIELD", "ELEMENTAL_DAMAGE_MOD", "START_MISSILE_ATTACK_TIMESTAMP", "LAST_RARE_USED_TIMESTAMP", "IGNORE_ARMOR", "PROC_SPELL_RATE", "RESISTANCE_MODIFIER", "ALLEGIANCE_GAG_TIMESTAMP", "ABSORB_MAGIC_DAMAGE", "CACHED_MAX_ABSORB_MAGIC_DAMAGE", "GAG_DURATION", "ALLEGIANCE_GAG_DURATION", "GLOBAL_XP_MOD", "HEALING_MODIFIER", "ARMOR_MOD_VS_NETHER", "RESIST_NETHER", "COOLDOWN_DURATION", "WEAPON_AURA_OFFENSE", "WEAPON_AURA_DEFENSE", "WEAPON_AURA_ELEMENTAL", "WEAPON_AURA_MANA_CONV"}

func (stat *FloatStat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypeFloat)) {
		stat_name = STypeFloat[stat.Key]
	}
	return fmt.Sprintf("%s (%d): %f", stat_name, stat.Key, stat.Value)
}

type Int64Stat struct {
	Key   uint32 `json:"key"`
	Value int64  `json:"value"`
}

var STypeInt64 []string = []string{"UNDEF", "TOTAL_EXPERIENCE", "AVAILABLE_EXPERIENCE", "AUGMENTATION_COST", "ITEM_TOTAL_XP", "ITEM_BASE_XP", "AVAILABLE_LUMINANCE", "MAXIMUM_LUMINANCE", "INTERACTION_REQS"}

func (stat *Int64Stat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypeInt64)) {
		stat_name = STypeInt64[stat.Key]
	}
	return fmt.Sprintf("%s (%d): %d", stat_name, stat.Key, stat.Value)
}

type StringStat struct {
	Key   uint32      `json:"key"`
	Value Dat_PString `json:"value"`
}

var STypeString []string = []string{"UNDEF", "NAME", "TITLE", "SEX", "HERITAGE_GROUP", "TEMPLATE", "ATTACKERS_NAME", "INSCRIPTION", "SCRIBE_NAME", "VENDORS_NAME", "FELLOWSHIP", "MONARCHS_NAME", "LOCK_CODE", "KEY_CODE", "USE", "SHORT_DESC", "LONG_DESC", "ACTIVATION_TALK", "USE_MESSAGE", "ITEM_HERITAGE_GROUP_RESTRICTION", "PLURAL_NAME", "MONARCHS_TITLE", "ACTIVATION_FAILURE", "SCRIBE_ACCOUNT", "TOWN_NAME", "CRAFTSMAN_NAME", "USE_PK_SERVER_ERROR", "SCORE_CACHED_TEXT", "SCORE_DEFAULT_ENTRY_FORMAT", "SCORE_FIRST_ENTRY_FORMAT", "SCORE_LAST_ENTRY_FORMAT", "SCORE_ONLY_ENTRY_FORMAT", "SCORE_NO_ENTRY", "QUEST", "GENERATOR_EVENT", "PATRONS_TITLE", "HOUSE_OWNER_NAME", "QUEST_RESTRICTION", "APPRAISAL_PORTAL_DESTINATION", "TINKER_NAME", "IMBUER_NAME", "HOUSE_OWNER_ACCOUNT", "DISPLAY_NAME", "DATE_OF_BIRTH", "THIRD_PARTY_API", "KILL_QUEST", "AFK", "ALLEGIANCE_NAME", "AUGMENTATION_ADD_QUEST", "KILL_QUEST_2", "KILL_QUEST_3", "USE_SENDS_SIGNAL", "GEAR_PLATING_NAME"}

func (stat *StringStat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypeString)) {
		stat_name = STypeString[stat.Key]
	}
	return fmt.Sprintf("%s (%d): \"%s\"", stat_name, stat.Key, stat.Value)
}

type PositionStat struct {
	Key   uint32   `json:"key"`
	Value Position `json:"value"`
}

var STypePosition []string = []string{"UNDEF", "LOCATION", "DESTINATION", "INSTANTIATION", "SANCTUARY", "HOME", "ACTIVATION_MOVE", "TARGET", "LINKED_PORTAL_ONE", "LAST_PORTAL", "PORTAL_STORM", "CRASH_AND_TURN", "PORTAL_SUMMON_LOC", "HOUSE_BOOT", "LAST_OUTSIDE_DEATH", "LINKED_LIFESTONE", "LINKED_PORTAL_TWO", "SAVE_1", "SAVE_2", "SAVE_3", "SAVE_4", "SAVE_5", "SAVE_6", "SAVE_7", "SAVE_8", "SAVE_9", "RELATIVE_DESTINATION", "TELEPORTED_CHARACTER"}

func (stat *PositionStat) ToString() string {
	var stat_name string = "Unknown"
	if stat.Key < uint32(len(STypePosition)) {
		stat_name = STypePosition[stat.Key]
	}
	return fmt.Sprintf("%s (%d): %s", stat_name, stat.Key, stat.Value.ToString())
}

type SkillStat struct {
	Key   uint32 `json:"key"`
	Value Skill  `json:"value"`
}

var STypeSkill []string = []string{"UNDEF", "AXE", "BOW", "CROSSBOW", "DAGGER", "MACE", "MELEE_DEFENSE", "MISSILE_DEFENSE", "SLING", "SPEAR", "STAFF", "SWORD", "THROWN_WEAPON", "UNARMED_COMBAT", "ARCANE_LORE", "MAGIC_DEFENSE", "MANA_CONVERSION", "SPELLCRAFT", "ITEM_APPRAISAL", "PERSONAL_APPRAISAL", "DECEPTION", "HEALING", "JUMP", "LOCKPICK", "RUN", "AWARENESS", "ARMS_AND_ARMOR_REPAIR", "CREATURE_APPRAISAL", "WEAPON_APPRAISAL", "ARMOR_APPRAISAL", "MAGIC_ITEM_APPRAISAL", "CREATURE_ENCHANTMENT", "ITEM_ENCHANTMENT", "LIFE_MAGIC", "WAR_MAGIC", "LEADERSHIP", "LOYALTY", "FLETCHING", "ALCHEMY", "COOKING", "SALVAGING", "TWO_HANDED_COMBAT", "GEARCRAFT", "VOID_MAGIC", "HEAVY_WEAPONS", "LIGHT_WEAPONS", "FINESSE_WEAPONS", "MISSILE_WEAPONS", "SHIELD", "DUAL_WIELD", "RECKLESSNESS", "SNEAK_ATTACK", "DIRTY_FIGHTING", "CHALLENGE", "SUMMONING"}

var WeenieType []string = []string{"Undef", "Generic", "Clothing", "MissileLauncher", "Missile", "Ammunition", "MeleeWeapon", "Portal", "Book", "Coin", "Creature", "Admin", "Vendor", "HotSpot", "Corpse", "Cow", "AI", "Machine", "Food", "Door", "Chest", "Container", "Key", "Lockpick", "PressurePlate", "LifeStone", "Switch", "PKModifier", "Healer", "LightSource", "Allegiance", "Type32", "SpellComponent", "ProjectileSpell", "Scroll", "Caster", "Channel", "ManaStone", "Gem", "AdvocateFane", "AdvocateItem", "Sentinel", "GSpellEconomy", "LSpellEconomy", "CraftTool", "LScoreKeeper", "GScoreKeeper", "GScoreGatherer", "ScoreBook", "EventCoordinator", "Entity", "Stackable", "HUD", "House", "Deed", "SlumLord", "Hook", "Storage", "BootSpot", "HousePortal", "Game", "GamePiece", "SkillAlterationDevice", "AttributeTransferDevice", "Hooker", "AllegianceBindstone", "InGameStatKeeper", "AugmentationDevice", "SocialManager", "Pet", "PetDevice", "CombatPet"}

type SpellBook struct {
	Key   uint32 `json:"key"`
	Value struct {
		CastingLikelihood float32 `json:"casting_likelihood"`
	} `json:"value"`
}

func (t *SpellBook) Read(s *SBuffer) *SpellBook {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (spell *SpellBook) ToString() string {
	var spell_name string = "Unknown"
	if spellhash, ok := SpellBaseHash[spell.Key]; ok {
		spell_name = string(spellhash.Value.Name)
	}
	return fmt.Sprintf("%s (%d): %f%%", spell_name, spell.Key, spell.Value.CastingLikelihood)
}

type EmoteTable struct {
	Key   EmoteCategoryKey `json:"key"`
	Value []EmoteCategory  `json:"value"`
}

func (t *EmoteTable) Read(s *SBuffer) *EmoteTable {
	t.Key = EmoteCategoryKey(*s.readUint32())
	t.Value = make([]EmoteCategory, *s.readUint32()&0xFFFF)
	for i := range t.Value {
		t.Value[i].Read(s)
	}
	return t
}

type EmoteCategory struct {
	Category    EmoteCategoryKey `json:"category"`
	Probability float32          `json:"probability"`
	ClassID     *uint32          `json:"classID,omitempty"`
	Style       *uint32          `json:"style,omitempty"`
	SubStyle    *uint32          `json:"substyle,omitempty"`
	VendorType  *VendorType      `json:"vendorType,omitempty"`
	Quest       *string          `json:"quest,omitempty"`
	MinHealth   *float32         `json:"minhealth,omitempty"`
	MaxHealth   *float32         `json:"maxhealth,omitempty"`
	Emotes      []Emote          `json:"emotes"`
}

func (t *EmoteCategory) Read(s *SBuffer) *EmoteCategory {
	t.Category = EmoteCategoryKey(*s.readUint32())
	t.Probability = *s.readSingle()
	switch t.Category {

	case 1: // Refuse_EmoteCategory
		fallthrough
	case 6: // Give_EmoteCategory
		t.ClassID = s.readUint32()

	case 5: // HeartBeat_EmoteCategory
		t.Style = s.readUint32()
		t.SubStyle = s.readUint32()

	case 12: // QuestSuccess_EmoteCategory
		fallthrough
	case 13: // QuestFailure_EmoteCategory
		fallthrough
	case 14: // TestSuccess_EmoteCategory
		fallthrough
	case 23: // TestFailure_EmoteCategory
		fallthrough
	case 27: // EventSuccess_EmoteCategory
		fallthrough
	case 28: // EventFailure_EmoteCategory
		fallthrough
	case 29: // TestNoQuality_EmoteCategory
		fallthrough
	case 30: // QuestNoFellow_EmoteCategory
		fallthrough
	case 31: // TestNoFellow_EmoteCategory
		fallthrough
	case 32: // GotoSet_EmoteCategory
		fallthrough
	case 33: // NumFellowsSuccess_EmoteCategory
		fallthrough
	case 34: // NumFellowsFailure_EmoteCategory
		fallthrough
	case 35: // NumCharacterTitlesSuccess_EmoteCategory
		fallthrough
	case 36: // NumCharacterTitlesFailure_EmoteCategory
		fallthrough
	case 37: // ReceiveLocalSignal_EmoteCategory
		fallthrough
	case 38: // ReceiveTalkDirect_EmoteCategory
		t.Quest = (*string)(new(Dat_PString).Read(s, false))

	case 2: // Vendor_EmoteCategory
		t.VendorType = (*VendorType)(s.readUint32())

	case 15: // WoundedTaunt_EmoteCategory
		t.MinHealth = s.readSingle()
		t.MaxHealth = s.readSingle()
	}

	t.Emotes = make([]Emote, *s.readUint32()&0xFFFF)
	for i := range t.Emotes {
		t.Emotes[i].Read(s)
	}
	return t
}

type VendorType uint32

var VendorTypeEnum []string = []string{"UNDEF", "OpenVendor", "LeaveVendor", "Sell", "Buy", "Motion"}

func (stat *VendorType) String() string {
	var stat_name string = "Unknown"
	if int(*stat) < len(VendorTypeEnum) {
		stat_name = VendorTypeEnum[int(*stat)]
	}
	return fmt.Sprintf("%s (%d)", stat_name, int(*stat))
}

func (c *EmoteCategory) ToString() string {
	var reta []string = []string{fmt.Sprintf("Probability: %f%%", 100*c.Probability)}
	if c.ClassID != nil {
		reta = append(reta, fmt.Sprintf("ClassID: %s (%d)", Weenies[*c.ClassID].GetName(), *c.ClassID))
	}
	if c.Quest != nil {
		reta = append(reta, fmt.Sprintf("Quest: \"%s\"", *c.Quest))
	}
	if c.Style != nil {
		reta = append(reta, fmt.Sprintf("Style: 0x%08X (%d)", *c.Style, *c.Style))
	}
	if c.SubStyle != nil {
		reta = append(reta, fmt.Sprintf("SubStyle: 0x%08X (%d)", *c.SubStyle, *c.SubStyle))
	}
	if c.Emotes != nil {
		var retb []string = []string{fmt.Sprintf("Emotes: %d", len(c.Emotes))}
		for _, e := range c.Emotes {
			retb = append(retb, fmt.Sprintf("\n\t\t\tEmote: %s", e.ToString()))
		}
		reta = append(reta, strings.Join(retb, ""))
	}
	return strings.Join(reta, ", ")
}

type EmoteCategoryKey int

var emoteCategory []string = []string{"Invalid", "Refuse", "Vendor", "Death", "Portal", "HeartBeat", "Give", "Use", "Activation", "Generation", "PickUp", "Drop", "QuestSuccess", "QuestFailure", "Taunt", "WoundedTaunt", "KillTaunt", "NewEnemy", "Scream", "Homesick", "ReceiveCritical", "ResistSpell", "TestSuccess", "TestFailure", "HearChat", "Wield", "UnWield", "EventSuccess", "EventFailure", "TestNoQuality", "QuestNoFellow", "TestNoFellow", "GotoSet", "NumFellowsSuccess", "NumFellowsFailure", "NumCharacterTitlesSuccess", "NumCharacterTitlesFailure", "ReceiveLocalSignal", "ReceiveTalkDirect"}

func (e EmoteCategoryKey) ToString() string {
	if int(e) < len(emoteCategory) {
		return emoteCategory[int(e)]
	}
	return "Invalid"
}

type Emote struct {
	Type   EmoteType `json:"type"`
	Delay  float32   `json:"delay"`
	Extent float32   `json:"extent"`

	Msg           *string          `json:"msg,omitempty"`
	Stat          *uint32          `json:"stat,omitempty"`
	Amount        *uint32          `json:"amount,omitempty"`
	Amount64      *uint64          `json:"amount64,omitempty"`
	Heroxp64      *uint64          `json:"heroxp64,omitempty"`
	Percent       *float64         `json:"percent,omitempty"`
	Min           *uint32          `json:"min,omitempty"`
	Max           *uint32          `json:"max,omitempty"`
	Motion        *uint32          `json:"motion,omitempty"`
	Sound         *SoundType       `json:"sound,omitempty"`
	SpellID       *uint32          `json:"spellid,omitempty"`
	CProf         *CreationProfile `json:"cprof,omitempty"`
	WealthRating  *uint32          `json:"wealth_rating,omitempty"`
	TreasureClass *uint32          `json:"treasure_class,omitempty"`
	TreasureType  *uint32          `json:"treasure_type,omitempty"`
	Frame         *Frame           `json:"frame,omitempty"`
	PScript       *PScriptType     `json:"pscript,omitempty"`
	Teststring    *string          `json:"teststring,omitempty"`
	Min64         *uint64          `json:"min64,omitempty"`
	Max64         *uint64          `json:"max64,omitempty"`
	FMin          *float64         `json:"fmin,omitempty"`
	FMax          *float64         `json:"fmax,omitempty"`
	Display       *int32           `json:"display,omitempty"`
	MPosition     *Position        `json:"mPosition,omitempty"`
}

func (t *Emote) Read(s *SBuffer) *Emote {
	t.Type = EmoteType(*s.readUint32())
	t.Delay = *s.readSingle()
	t.Extent = *s.readSingle()
	switch t.Type {
	case 0x01:
		fallthrough
	case 0x08:
		fallthrough
	case 0x0A:
		fallthrough
	case 0x0D:
		fallthrough
	case 0x10:
		fallthrough
	case 0x11:
		fallthrough
	case 0x12:
		fallthrough
	case 0x14:
		fallthrough
	case 0x15:
		fallthrough
	case 0x16:
		fallthrough
	case 0x17:
		fallthrough
	case 0x18:
		fallthrough
	case 0x19:
		fallthrough
	case 0x1A:
		fallthrough
	case 0x1F:
		fallthrough
	case 0x33:
		fallthrough
	case 0x3A:
		fallthrough
	case 0x3C:
		fallthrough
	case 0x3D:
		fallthrough
	case 0x40:
		fallthrough
	case 0x41:
		fallthrough
	case 0x43:
		fallthrough
	case 0x44:
		fallthrough
	case 0x4F:
		fallthrough
	case 0x50:
		fallthrough
	case 0x51:
		fallthrough
	case 0x53:
		fallthrough
	case 0x58:
		fallthrough
	case 0x79:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
	case 0x20:
		fallthrough
	case 0x21:
		fallthrough
	case 0x46:
		fallthrough
	case 0x54:
		fallthrough
	case 0x55:
		fallthrough
	case 0x56:
		fallthrough
	case 0x59:
		fallthrough
	case 0x66:
		fallthrough
	case 0x67:
		fallthrough
	case 0x68:
		fallthrough
	case 0x69:
		fallthrough
	case 0x6A:
		fallthrough
	case 0x6B:
		fallthrough
	case 0x6C:
		fallthrough
	case 0x6D:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.Amount = s.readUint32()
	case 0x35:
		fallthrough
	case 0x36:
		fallthrough
	case 0x37:
		fallthrough
	case 0x45:
		t.Stat = s.readUint32()
		t.Amount = s.readUint32()
	case 0x73:
		t.Stat = s.readUint32()
		t.Amount64 = s.readUint64()
	case 0x76:
		t.Stat = s.readUint32()
		t.Percent = s.readDouble()
	case 0x1E:
		fallthrough
	case 0x3B:
		fallthrough
	case 0x47:
		fallthrough
	case 0x52:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.Min = s.readUint32()
		t.Max = s.readUint32()
	case 0x02: // AwardXP_EmoteType
		fallthrough
	case 0x3E: // AwardNoShareXP_EmoteType
		t.Amount64 = s.readUint64()
		t.Heroxp64 = s.readUint64()
	case 0x70:
		fallthrough
	case 0x71:
		t.Amount64 = s.readUint64()
		t.Heroxp64 = s.readUint64()
	case 0x22:
		fallthrough
	case 0x2F:
		fallthrough
	case 0x30:
		fallthrough
	case 0x5A:
		fallthrough
	case 0x77:
		fallthrough
	case 0x78:
		t.Amount = s.readUint32()
	case 0x0E:
		fallthrough
	case 0x13:
		fallthrough
	case 0x1B:
		fallthrough
	case 0x49:
		t.SpellID = s.readUint32()
	case 3: // Give_EmoteType
		fallthrough
	case 74: // TakeItems_EmoteType
		t.CProf = new(CreationProfile).Read(s)
	case 76: // InqOwnsItems_EmoteType
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.CProf = new(CreationProfile).Read(s)
	case 56: // CreateTreasure_EmoteType
		t.WealthRating = s.readUint32()
		t.TreasureClass = s.readUint32()
		t.TreasureType = s.readUint32()
	case 5: // Motion_EmoteType
		fallthrough
	case 52: // ForceMotion_EmoteType
		t.Motion = s.readUint32()
	case 4:
		fallthrough
	case 6:
		fallthrough
	case 0x0B:
		fallthrough
	case 0x57:
		t.Frame = new(Frame).Read(s)
	case 7:
		t.PScript = (*PScriptType)(s.readUint32())
	case 9:
		t.Sound = (*SoundType)(s.readUint32())
	case 0x1C:
		fallthrough
	case 0x1D:
		t.Amount = s.readUint32()
		t.Stat = s.readUint32()
	case 0x6E:
		t.Stat = s.readUint32()
	case 0x6F:
		t.Amount = s.readUint32()
	case 0x23:
		fallthrough
	case 0x2D:
		fallthrough
	case 0x2E:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.Stat = s.readUint32()
	case 0x26:
		fallthrough
	case 0x4B:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.Teststring = (*string)(new(Dat_PString).Read(s, false))
		t.Stat = s.readUint32()
	case 0x24:
		fallthrough
	case 0x27:
		fallthrough
	case 0x28:
		fallthrough
	case 0x29:
		fallthrough
	case 0x2A:
		fallthrough
	case 0x2B:
		fallthrough
	case 0x2C:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.Min = s.readUint32()
		t.Max = s.readUint32()
		t.Stat = s.readUint32()
	case 0x72:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.Min64 = s.readUint64()
		t.Max64 = s.readUint64()
		t.Stat = s.readUint32()
	case 0x25:
		t.Msg = (*string)(new(Dat_PString).Read(s, false))
		t.FMin = s.readDouble()
		t.FMax = s.readDouble()
		t.Stat = s.readUint32()
	case 0x31:
		t.Percent = s.readDouble()
		t.Min64 = s.readUint64()
		t.Max64 = s.readUint64()
		t.Display = s.readInt32()
	case 0x32:
		t.Stat = s.readUint32()
		t.Percent = s.readDouble()
		t.Min = s.readUint32()
		t.Max = s.readUint32()
		t.Display = s.readInt32()
	case 0x3F:
		t.MPosition = new(Position).Read(s)
	case 0x63:
		fallthrough
	case 0x64:
		t.MPosition = new(Position).Read(s)
	case 72: // Generate_EmoteType:
		t.Amount = s.readUint32()
	}
	return t
}

var SoundTypeEnum []string = []string{"Invalid", "Speak1", "Random", "Attack1", "Attack2", "Attack3", "SpecialAttack1", "SpecialAttack2", "SpecialAttack3", "Damage1", "Damage2", "Damage3", "Wound1", "Wound2", "Wound3", "Death1", "Death2", "Death3", "Grunt1", "Grunt2", "Grunt3", "Oh1", "Oh2", "Oh3", "Heave1", "Heave2", "Heave3", "Knockdown1", "Knockdown2", "Knockdown3", "Swoosh1", "Swoosh2", "Swoosh3", "Thump1", "Smash1", "Scratch1", "Spear", "Sling", "Dagger", "ArrowWhiz1", "ArrowWhiz2", "CrossbowPull", "CrossbowRelease", "BowPull", "BowRelease", "ThrownWeaponRelease1", "ArrowLand", "Collision", "HitFlesh1", "HitLeather1", "HitChain1", "HitPlate1", "HitMissile1", "HitMissile2", "HitMissile3", "Footstep1", "Footstep2", "Walk1", "Dance1", "Dance2", "Dance3", "Hidden1", "Hidden2", "Hidden3", "Eat1", "Drink1", "Open", "Close", "OpenSlam", "CloseSlam", "Ambient1", "Ambient2", "Ambient3", "Ambient4", "Ambient5", "Ambient6", "Ambient7", "Ambient8", "Waterfall", "LogOut", "LogIn", "LifestoneOn", "AttribUp", "AttribDown", "SkillUp", "SkillDown", "HealthUp", "HealthDown", "ShieldUp", "ShieldDown", "EnchantUp", "EnchantDown", "VisionUp", "VisionDown", "Fizzle", "Launch", "Explode", "TransUp", "TransDown", "BreatheFlaem", "BreatheAcid", "BreatheFrost", "BreatheLightning", "Create", "Destroy", "Lockpicking", "UI_EnterPortal", "UI_ExitPortal", "UI_GeneralQuery", "UI_GeneralError", "UI_TransientMessage", "UI_IconPickUp", "UI_IconSuccessfulDrop", "UI_IconInvalid_Drop", "UI_ButtonPress", "UI_GrabSlider", "UI_ReleaseSlider", "UI_NewTargetSelected", "UI_Roar", "UI_Bell", "UI_Chant1", "UI_Chant2", "UI_DarkWhispers1", "UI_DarkWhispers2", "UI_DarkLaugh", "UI_DarkWind", "UI_DarkSpeech", "UI_Drums", "UI_GhostSpeak", "UI_Breathing", "UI_Howl", "UI_LostSouls", "UI_Squeal", "UI_Thunder1", "UI_Thunder2", "UI_Thunder3", "UI_Thunder4", "UI_Thunder5", "UI_Thunder6", "RaiseTrait", "WieldObject", "UnwieldObject", "ReceiveItem", "PickUpItem", "DropItem", "ResistSpell", "PicklockFail", "LockSuccess", "OpenFailDueToLock", "TriggerActivated", "SpellExpire", "ItemManaDepleted", "TriggerActivated1", "TriggerActivated2", "TriggerActivated3", "TriggerActivated4", "TriggerActivated5", "TriggerActivated6", "TriggerActivated7", "TriggerActivated8", "TriggerActivated9", "TriggerActivated10", "TriggerActivated11", "TriggerActivated12", "TriggerActivated13", "TriggerActivated14", "TriggerActivated15", "TriggerActivated16", "TriggerActivated17", "TriggerActivated18", "TriggerActivated19", "TriggerActivated20", "TriggerActivated21", "TriggerActivated22", "TriggerActivated23", "TriggerActivated24", "TriggerActivated25", "TriggerActivated26", "TriggerActivated27", "TriggerActivated28", "TriggerActivated29", "TriggerActivated30", "TriggerActivated31", "TriggerActivated32", "TriggerActivated33", "TriggerActivated34", "TriggerActivated35", "TriggerActivated36", "TriggerActivated37", "TriggerActivated38", "TriggerActivated39", "TriggerActivated40", "TriggerActivated41", "TriggerActivated42", "TriggerActivated43", "TriggerActivated44", "TriggerActivated45", "TriggerActivated46", "TriggerActivated47", "TriggerActivated48", "TriggerActivated49", "TriggerActivated50", "HealthDownVoid", "RegenDownVoid", "SkillDownVoid"}

func (e SoundType) ToString() string {
	if int(e) < len(SoundTypeEnum) {
		return SoundTypeEnum[int(e)]
	}
	return "Invalid"
}

type SoundType uint32

var PScriptTypeEnum []string = []string{"Invalid", "Test1", "Test2", "Test3", "Launch", "Explode", "AttribUpRed", "AttribDownRed", "AttribUpOrange", "AttribDownOrange", "AttribUpYellow", "AttribDownYellow", "AttribUpGreen", "AttribDownGreen", "AttribUpBlue", "AttribDownBlue", "AttribUpPurple", "AttribDownPurple", "SkillUpRed", "SkillDownRed", "SkillUpOrange", "SkillDownOrange", "SkillUpYellow", "SkillDownYellow", "SkillUpGreen", "SkillDownGreen", "SkillUpBlue", "SkillDownBlue", "SkillUpPurple", "SkillDownPurple", "SkillDownBlack", "HealthUpRed", "HealthDownRed", "HealthUpBlue", "HealthDownBlue", "HealthUpYellow", "HealthDownYellow", "RegenUpRed", "RegenDownREd", "RegenUpBlue", "RegenDownBlue", "RegenUpYellow", "RegenDownYellow", "ShieldUpRed", "ShieldDownRed", "ShieldUpOrange", "ShieldDownOrange", "ShieldUpYellow", "ShieldDownYellow", "ShieldUpGreen", "ShieldDownGreen", "ShieldUpBlue", "ShieldDownBlue", "ShieldUpPurple", "ShieldDownPurple", "ShieldUpGrey", "ShieldDownGrey", "EnchantUpRed", "EnchantDownRed", "EnchantUpOrange", "EnchantDownOrange", "EnchantUpYellow", "EnchantDownYellow", "EnchantUpGreen", "EnchantDownGreen", "EnchantUpBlue", "EnchantDownBlue", "EnchantUpPurple", "EnchantDownPurple", "VitaeUpWhite", "VitaeDownBlack", "VisionUpWhite", "VisionDownBlack", "SwapHealth_Red_To_Yellow", "SwapHealth_Red_To_Blue", "SwapHealth_Yellow_To_Red", "SwapHealth_Yellow_To_Blue", "SwapHealth_Blue_To_Red", "SwapHealth_Blue_To_Yellow", "TransUpWhite", "TransDownBlack", "Fizzle", "PortalEntry", "PortalExit", "BreatheFlame", "BreatheFrost", "BreatheAcid", "BreatheLightning", "Create", "Destroy", "ProjectileCollision", "SplatterLowLeftBack", "SplatterLowLeftFront", "SplatterLowRightBack", "SplatterLowRightFront", "SplatterMidLeftBack", "SplatterMidLeftFront", "SplatterMidRightBack", "SplatterMidRightFront", "SplatterUpLeftBack", "SplatterUpLeftFront", "SplatterUpRightBack", "SplatterUpRightFront", "SparkLowLeftBack", "SparkLowLeftFront", "SparkLowRightBack", "SparkLowRightFront", "SparkMidLeftBack", "SparkMidLeftFront", "SparkMidRightBack", "SparkMidRightFront", "SparkUpLeftBack", "SparkUpLeftFront", "SparkUpRightBack", "SparkUpRightFront", "PortalStorm", "Hide", "UnHide", "Hidden", "DisappearDestroy", "cialState1", "cialState2", "cialState3", "cialState4", "cialState5", "cialState6", "cialState7", "cialState8", "cialState9", "cialState0", "cialStateRed", "cialStateOrange", "cialStateYellow", "cialStateGreen", "cialStateBlue", "cialStatePurple", "cialStateWhite", "cialStateBlack", "LevelUp", "EnchantUpGrey", "EnchantDownGrey", "WeddingBliss", "EnchantUpWhite", "EnchantDownWhite", "CampingMastery", "CampingIneptitude", "DispelLife", "DispelCreature", "DispelAll", "BunnySmite", "BaelZharonSmite", "WeddingSteele", "RestrictionEffectBlue", "RestrictionEffectGreen", "RestrictionEffectGold", "LayingofHands", "AugmentationUseAttribute", "AugmentationUseSkill", "AugmentationUseResistances", "AugmentationUseOther", "BlackMadness", "AetheriaLevelUp", "AetheriaSurgeDestruction", "AetheriaSurgeProtection", "AetheriaSurgeRegeneration", "AetheriaSurgeAffliction", "AetheriaSurgeFestering", "HealthDownVoid", "RegenDownVoid", "SkillDownVoid", "DirtyFightingHealDebuff", "DirtyFightingAttackDebuff", "DirtyFightingDefenseDebuff", "DirtyFightingDamageOverTime"}

func (e PScriptType) ToString() string {
	if int(e) < len(PScriptTypeEnum) {
		return PScriptTypeEnum[int(e)]
	}
	return "Invalid"
}

type PScriptType uint32

type CreationProfile struct {
	WCID        uint32      `json:"wcid"`
	Palette     uint32      `json:"palette"`
	Shade       float32     `json:"shade"`
	Destination Destination `json:"destination"`
	StackSize   int32       `json:"stack_size"`
	TryToBond   int32       `json:"try_to_bond"`
}

func (t *CreationProfile) Read(s *SBuffer) *CreationProfile {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

func (prof *CreationProfile) ToString() string {
	var weenie_name string = "Unknown"
	if weenie, ok := Weenies[prof.WCID]; ok {
		weenie_name = weenie.GetName()
	}
	return fmt.Sprintf("%s (%d) Pal:%d Shade:%f Dest: %s StackSize: %d TryToBond: %d",
		weenie_name, prof.WCID,
		prof.Palette, prof.Shade, prof.Destination.ToString(), prof.StackSize, prof.TryToBond,
	)
}

type Destination int32

func (dest Destination) ToString() string {
	var reta []string = make([]string, 0)
	if dest&1 != 0 {
		reta = append(reta, "Contain")
	}
	if dest&2 != 0 {
		reta = append(reta, "Wield")
	}
	if dest&4 != 0 {
		reta = append(reta, "Shop")
	}
	if dest&8 != 0 {
		reta = append(reta, "Treasure")
	}
	if dest&0x10 != 0 {
		reta = append(reta, "HouseBuy")
	}
	if dest&0x20 != 0 {
		reta = append(reta, "HouseRent")
	}
	if len(reta) == 0 {
		return "Undef"
	}
	return strings.Join(reta, "")
}
func (e *Emote) ToString() string {
	var reta []string = []string{}
	if e.Msg != nil {
		reta = append(reta, fmt.Sprintf("Msg: \"%s\"", *e.Msg))
	}
	if e.Amount != nil {
		reta = append(reta, fmt.Sprintf("Amount: %d", *e.Amount))
	}
	if e.Amount64 != nil {
		reta = append(reta, fmt.Sprintf("Amount64: %d", *e.Amount64))
	}
	if e.Heroxp64 != nil {
		reta = append(reta, fmt.Sprintf("Heroxp64: %d", *e.Heroxp64))
	}
	if e.Percent != nil {
		reta = append(reta, fmt.Sprintf("Percent: %f%%", *e.Percent))
	}
	if e.Min != nil {
		reta = append(reta, fmt.Sprintf("Min: %d", *e.Min))
	}
	if e.Max != nil {
		reta = append(reta, fmt.Sprintf("Max: %d", *e.Max))
	}
	if e.Stat != nil {
		reta = append(reta, fmt.Sprintf("Stat: %d", *e.Stat))
	}
	if e.Motion != nil {
		reta = append(reta, fmt.Sprintf("Motion: 0x%08X (%d)", *e.Motion, *e.Motion))
	}
	if e.Sound != nil {
		reta = append(reta, fmt.Sprintf("Sound: 0x%08X (%d): %s", *e.Sound, *e.Sound, e.Sound.ToString()))
	}
	if e.SpellID != nil {
		var spell_name string = "Unknown"
		if spellhash, ok := SpellBaseHash[*e.SpellID]; ok {
			spell_name = string(spellhash.Value.Name)
		}
		reta = append(reta, fmt.Sprintf("Spell: %s (%d)", spell_name, *e.SpellID))
	}
	if e.CProf != nil {
		reta = append(reta, fmt.Sprintf("CProf: %s", e.CProf.ToString()))
	}
	if e.WealthRating != nil {
		reta = append(reta, fmt.Sprintf("WealthRating: 0x%08X (%d)", *e.WealthRating, *e.WealthRating))
	}
	if e.TreasureClass != nil {
		reta = append(reta, fmt.Sprintf("TreasureClass: 0x%08X (%d)", *e.TreasureClass, *e.TreasureClass))
	}
	if e.TreasureType != nil {
		reta = append(reta, fmt.Sprintf("TreasureType: 0x%08X (%d)", *e.TreasureType, *e.TreasureType))
	}
	if e.Frame != nil {
		reta = append(reta, fmt.Sprintf("Frame: %s", e.Frame.ToString()))
	}
	if e.PScript != nil {
		reta = append(reta, fmt.Sprintf("PScript: 0x%08X (%d): %s", *e.PScript, *e.PScript, e.PScript.ToString()))
	}
	if e.Teststring != nil {
		reta = append(reta, fmt.Sprintf("Teststring: \"%s\"", *e.Teststring))
	}
	if e.Min64 != nil {
		reta = append(reta, fmt.Sprintf("Min64: %d", *e.Min64))
	}
	if e.Max64 != nil {
		reta = append(reta, fmt.Sprintf("Max64: %d", *e.Max64))
	}
	if e.FMin != nil {
		reta = append(reta, fmt.Sprintf("FMin: %f", *e.FMin))
	}
	if e.FMax != nil {
		reta = append(reta, fmt.Sprintf("FMax: %f", *e.FMax))
	}
	if e.Display != nil {
		reta = append(reta, fmt.Sprintf("Display: %d", *e.Display))
	}
	if e.MPosition != nil {
		reta = append(reta, fmt.Sprintf("MPosition: %s", e.MPosition.ToString()))
	}
	return fmt.Sprintf("%s (%d) Delay: %f, Extent: %f\n\t\t\t\t%s", e.Type.ToString(), int(e.Type), e.Delay, e.Extent, strings.Join(reta, ", "))
}

type EmoteType int

var emoteType []string = []string{"Invalid", "Act", "AwardXP", "Give", "MoveHome", "Motion", "Move", "PhysScript", "Say", "Sound", "Tell", "Turn", "TurnToTarget", "TextDirect", "CastSpell", "Activate", "WorldBroadcast", "LocalBroadcast", "DirectBroadcast", "CastSpellInstant", "UpdateQuest", "InqQuest", "StampQuest", "StartEvent", "StopEvent", "BLog", "AdminSpam", "TeachSpell", "AwardSkillXP", "AwardSkillPoints", "InqQuestSolves", "EraseQuest", "DecrementQuest", "IncrementQuest", "AddCharacterTitle", "InqBoolStat", "InqIntStat", "InqFloatStat", "InqStringStat", "InqAttributeStat", "InqRawAttributeStat", "InqSecondaryAttributeStat", "InqRawSecondaryAttributeStat", "InqSkillStat", "InqRawSkillStat", "InqSkillTrained", "InqSkillSpecialized", "AwardTrainingCredits", "InflictVitaePenalty", "AwardLevelProportionalXP", "AwardLevelProportionalSkillXP", "InqEvent", "ForceMotion", "SetIntStat", "IncrementIntStat", "DecrementIntStat", "CreateTreasure", "ResetHomePosition", "InqFellowQuest", "InqFellowNum", "UpdateFellowQuest", "StampFellowQuest", "AwardNoShareXP", "SetSanctuaryPosition", "TellFellow", "FellowBroadcast", "LockFellow", "Goto", "PopUp", "SetBoolStat", "SetQuestCompletions", "InqNumCharacterTitles", "Generate", "PetCastSpellOnOwner", "TakeItems", "InqYesNo", "InqOwnsItems", "DeleteSelf", "KillSelf", "UpdateMyQuest", "InqMyQuest", "StampMyQuest", "InqMyQuestSolves", "EraseMyQuest", "DecrementMyQuest", "IncrementMyQuest", "SetMyQuestCompletions", "MoveToPos", "LocalSignal", "InqPackSpace", "RemoveVitaePenalty", "SetEyeTexture", "SetEyePalette", "SetNoseTexture", "SetNosePalette", "SetMouthTexture", "SetMouthPalette", "SetHeadObject", "SetHeadPalette", "TeleportTarget", "TeleportSelf", "StartBarber", "InqQuestBitsOn", "InqQuestBitsOff", "InqMyQuestBitsOn", "InqMyQuestBitsOff", "SetQuestBitsOn", "SetQuestBitsOff", "SetMyQuestBitsOn", "SetMyQuestBitsOff", "UntrainSkill", "SetAltRacialSkills", "SpendLuminance", "AwardLuminance", "InqInt64Stat", "SetInt64Stat", "OpenMe", "CloseMe", "SetFloatStat", "AddContract", "RemoveContract", "InqContractsFull"}

func (e EmoteType) ToString() string {
	if int(e) < len(emoteType) {
		return emoteType[int(e)]
	}
	return "Invalid"
}

type Generator struct {
	Probability float32           `json:"probability"`
	Type        uint32            `json:"type"`
	Delay       float64           `json:"delay"`
	InitCreate  int32             `json:"initCreate"`
	MaxNum      int32             `json:"maxNum"`
	WhenCreate  RegenerationType  `json:"whenCreate"`
	WhereCreate RegenLocationType `json:"whereCreate"`
	StackSize   int32             `json:"stackSize"`
	PTID        uint32            `json:"ptid"`
	Shade       float32           `json:"shade"`
	Position
	Slot uint32 `json:"slot"`
}

func (t *Generator) Read(s *SBuffer) *Generator {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type RegenerationType int32
type RegenLocationType int32
