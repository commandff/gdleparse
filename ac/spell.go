package ac

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"sync"
)

var SpellBaseHash_m sync.Mutex
var SpellBaseHash = make(map[uint32]SpellBase)
var SpellBaseHash_IDs = make([]uint32, 0)

func SpellBaseHash_Stats() {
	firstSpellID := SpellBaseHash_IDs[0]
	lastSpellID := SpellBaseHash_IDs[len(SpellBaseHash_IDs)-1]
	fmt.Printf("Total Spells: %d == %d, %s (%d) thru %s (%d)\n",
		len(SpellBaseHash), len(SpellBaseHash_IDs),
		SpellBaseHash[firstSpellID].Value.Name, firstSpellID,
		SpellBaseHash[lastSpellID].Value.Name, lastSpellID,
	)
}

func SpellBaseHash_Parse(root string) error {
	SpellBaseHash_m.Lock()
	defer SpellBaseHash_m.Unlock()
	SpellBaseHash = make(map[uint32]SpellBase)
	SpellBaseHash_IDs = make([]uint32, 0)
	parseSpells(root)

	sort.SliceStable(SpellBaseHash_IDs, func(i, j int) bool {
		return SpellBaseHash_IDs[i] < SpellBaseHash_IDs[j]
	})

	return nil
}
func parseSpells(root string) {
	file := filepath.Join(root, "spells.json")
	content, err := os.ReadFile(file)
	if err == nil {
		var w SpellTable
		if err := json.Unmarshal(content, &w); err != nil {
			log.Printf("ERROR: failed to parse %s: %v", file, err)
			return
		}
		for _, spellbase := range w.Table.SpellBaseHash {
			SpellBaseHash[spellbase.Key] = spellbase
			SpellBaseHash_IDs = append(SpellBaseHash_IDs, spellbase.Key)
		}
	} else {
		fmt.Printf("ERROR: Unable to read spells.json(%s): %s\n", file, err)
	}
}

type SpellTable struct {
	TotalObjects uint16 `json:"-"`
	Table        struct {
		SpellBaseHash []SpellBase `json:"spellBaseHash"`
	} `json:"table"`
}

func (t *SpellTable) Read(s *SBuffer) *SpellTable {
	t.TotalObjects = *s.readUint16()
	t.Table.SpellBaseHash = make([]SpellBase, t.TotalObjects)
	for i := range t.Table.SpellBaseHash {
		t.Table.SpellBaseHash[i].Read(s)
	}
	return t
}

type SpellBase struct {
	Key   uint32 `json:"key"`
	Value struct {
		Name           Dat_PString `json:"name"`
		Desc           Dat_PString `json:"desc"`
		School         int32       `json:"school"`
		IconID         uint32      `json:"iconID"`
		Category       uint32      `json:"category"`
		Bitfield       uint32      `json:"bitfield"`
		Mana           uint32      `json:"base_mana"`
		RangeConstant  float32     `json:"base_range_constant"`
		RangeMod       float32     `json:"base_range_mod"`
		Power          uint32      `json:"power"`
		EconomyMod     float32     `json:"spell_economy_mod"`
		FormulaVersion uint32      `json:"formula_version"`
		ComponentLoss  float32     `json:"component_loss"`
		MetaSpell      struct {
			SpType int32 `json:"sp_type"`
			Spell  struct {
				SpellID uint32 `json:"spell_id"`
				// SPType_1 || SPType_12 || SPType_15
				DegradeLimit    *float32 `json:"degrade_limit,omitempty"`
				DegradeModifier *float32 `json:"degrade_modifier,omitempty"`
				Duration        *float64 `json:"duration,omitempty"`
				SpellCategory   *uint32  `json:"spellCategory,omitempty"`
				Smod            *StatMod `json:"smod,omitempty"`
				// SPType_2 || SPType_10 || SPType_15
				EType                  *uint32  `json:"etype,omitempty"`
				BaseIntensity          *int32   `json:"baseIntensity,omitempty"`
				Variance               *int32   `json:"variance,omitempty"`
				WCID                   *uint32  `json:"wcid,omitempty"`
				NumProjectiles         *int32   `json:"numProjectiles,omitempty"`
				NumProjectilesVariance *int32   `json:"numProjectilesVariance,omitempty"`
				SpreadAngle            *float32 `json:"spreadAngle,omitempty"`
				VerticalAngle          *float32 `json:"verticalAngle,omitempty"`
				DefaultLaunchAngle     *float32 `json:"defaultLaunchAngle,omitempty"`
				NonTracking            *int32   `json:"bNonTracking,omitempty"`
				CreateOffset           *Vector3 `json:"createOffset,omitempty"`
				Padding                *Vector3 `json:"padding,omitempty"`
				Dims                   *Vector3 `json:"dims,omitempty"`
				Peturbation            *Vector3 `json:"peturbation,omitempty"`
				ImbuedEffect           *uint32  `json:"imbuedEffect,omitempty"`
				SlayerCreatureType     *int32   `json:"slayerCreatureType,omitempty"`
				SlayerDamageBonus      *float32 `json:"slayerDamageBonus,omitempty"`
				CritFreq               *float64 `json:"critFreq,omitempty"`
				CritMultiplier         *float64 `json:"critMultiplier,omitempty"`
				IgnoreMagicResist      *int32   `json:"ignoreMagicResist,omitempty"`
				ElementalModifier      *float64 `json:"elementalModifier,omitempty"`
				// SPType_10
				DrainPercentage *float32 `json:"drain_percentage,omitempty"`
				DamageRatio     *float32 `json:"damage_ratio,omitempty"`
				// SPType_3 || SPType_11
				DamageType    *uint32 `json:"dt,omitempty"`
				Boost         *int32  `json:"boost,omitempty"`
				BoostVariance *int32  `json:"boostVariance,omitempty"`
				// SPType_4
				Source           *int32   `json:"src,omitempty"`
				Destination      *int32   `json:"dest,omitempty"`
				Proportion       *float32 `json:"proportion,omitempty"`
				LossPercent      *float32 `json:"lossPercent,omitempty"`
				SourceLoss       *int32   `json:"sourceLoss,omitempty"`
				TransferCap      *int32   `json:"transferCap,omitempty"`
				MaxBoostAllowed  *int32   `json:"maxBoostAllowed,omitempty"`
				TransferBitfield *uint32  `json:"bitfield,omitempty"`
				// SPType_5 || SPType_6
				Index *int32 `json:"index,omitempty"`
				// SPType_7
				PortalLifetime *float64 `json:"portal_lifetime,omitempty"`
				Link           *int32   `json:"link,omitempty"`
				// SPType_8 || SPType_13
				POS *Position `json:"pos,omitempty"`
				// SPType_9 || SPType_14
				MinPower       *int32   `json:"min_power,omitempty"`
				MaxPower       *int32   `json:"max_power,omitempty"`
				PowerVariance  *float32 `json:"power_variance,omitempty"`
				School         *int32   `json:"school,omitempty"`
				Align          *int32   `json:"align,omitempty"`
				Number         *int32   `json:"number,omitempty"`
				NumberVariance *float32 `json:"number_variance,omitempty"`
			} `json:"spell"`
		} `json:"meta_spell"`
		Formula                []uint32 `json:"formula"`
		CasterEffect           uint32   `json:"caster_effect"`
		TargetEffect           uint32   `json:"target_effect"`
		FizzleEffect           uint32   `json:"fizzle_effect"`
		RecoveryInterval       float64  `json:"recovery_interval"`
		RecoveryAmount         float32  `json:"recovery_amount"`
		DisplayOrder           uint32   `json:"display_order"`
		NonComponentTargetType uint32   `json:"non_component_target_type"`
		ManaMod                uint32   `json:"mana_mod"`
	} `json:"value"`
	general
}

func (t *SpellBase) Read(s *SBuffer) *SpellBase {
	t.Key = *s.readUint32()
	t.Value.Name = *new(Dat_PString).Read(s, false)
	t.Value.Desc = *new(Dat_PString).Read(s, false)
	t.Value.School = *s.readInt32()
	t.Value.IconID = *s.readUint32()
	t.Value.Category = *s.readUint32()
	t.Value.Bitfield = *s.readUint32()
	t.Value.Mana = *s.readUint32()
	t.Value.RangeConstant = *s.readSingle()
	t.Value.RangeMod = *s.readSingle()
	t.Value.Power = *s.readUint32()
	t.Value.EconomyMod = *s.readSingle()
	t.Value.FormulaVersion = *s.readUint32()
	t.Value.ComponentLoss = *s.readSingle()
	t.Value.MetaSpell.SpType = *s.readInt32()
	t.Value.MetaSpell.Spell.SpellID = *s.readUint32()
	if t.Value.MetaSpell.SpType == 1 || t.Value.MetaSpell.SpType == 12 || t.Value.MetaSpell.SpType == 15 {
		t.Value.MetaSpell.Spell.Duration = s.readDouble()
		t.Value.MetaSpell.Spell.DegradeModifier = s.readSingle()
		t.Value.MetaSpell.Spell.DegradeLimit = s.readSingle()
		t.Value.MetaSpell.Spell.SpellCategory = s.readUint32()
		t.Value.MetaSpell.Spell.Smod = new(StatMod).Read(s)
	}
	if t.Value.MetaSpell.SpType == 2 || t.Value.MetaSpell.SpType == 10 || t.Value.MetaSpell.SpType == 15 {
		t.Value.MetaSpell.Spell.EType = s.readUint32()
		t.Value.MetaSpell.Spell.BaseIntensity = s.readInt32()
		t.Value.MetaSpell.Spell.Variance = s.readInt32()
		t.Value.MetaSpell.Spell.WCID = s.readUint32()
		t.Value.MetaSpell.Spell.NumProjectiles = s.readInt32()
		t.Value.MetaSpell.Spell.NumProjectilesVariance = s.readInt32()
		t.Value.MetaSpell.Spell.SpreadAngle = s.readSingle()
		t.Value.MetaSpell.Spell.VerticalAngle = s.readSingle()
		t.Value.MetaSpell.Spell.DefaultLaunchAngle = s.readSingle()
		t.Value.MetaSpell.Spell.NonTracking = s.readInt32()

		t.Value.MetaSpell.Spell.CreateOffset = new(Vector3).Read(s)
		t.Value.MetaSpell.Spell.Padding = new(Vector3).Read(s)
		t.Value.MetaSpell.Spell.Dims = new(Vector3).Read(s)
		t.Value.MetaSpell.Spell.Peturbation = new(Vector3).Read(s)

		t.Value.MetaSpell.Spell.ImbuedEffect = s.readUint32()
		t.Value.MetaSpell.Spell.SlayerCreatureType = s.readInt32()
		t.Value.MetaSpell.Spell.SlayerDamageBonus = s.readSingle()
		t.Value.MetaSpell.Spell.CritFreq = s.readDouble()
		t.Value.MetaSpell.Spell.CritMultiplier = s.readDouble()
		t.Value.MetaSpell.Spell.IgnoreMagicResist = s.readInt32()
		t.Value.MetaSpell.Spell.ElementalModifier = s.readDouble()
		if t.Value.MetaSpell.SpType == 10 {
			t.Value.MetaSpell.Spell.DrainPercentage = s.readSingle()
			t.Value.MetaSpell.Spell.DamageRatio = s.readSingle()
		}
	}
	if t.Value.MetaSpell.SpType == 3 || t.Value.MetaSpell.SpType == 11 {
		t.Value.MetaSpell.Spell.DamageType = s.readUint32()
		t.Value.MetaSpell.Spell.Boost = s.readInt32()
		t.Value.MetaSpell.Spell.BoostVariance = s.readInt32()
	}
	if t.Value.MetaSpell.SpType == 4 {
		t.Value.MetaSpell.Spell.Source = s.readInt32()
		t.Value.MetaSpell.Spell.Destination = s.readInt32()
		t.Value.MetaSpell.Spell.Proportion = s.readSingle()
		t.Value.MetaSpell.Spell.LossPercent = s.readSingle()
		t.Value.MetaSpell.Spell.SourceLoss = s.readInt32()
		t.Value.MetaSpell.Spell.TransferCap = s.readInt32()
		t.Value.MetaSpell.Spell.MaxBoostAllowed = s.readInt32()
		t.Value.MetaSpell.Spell.TransferBitfield = s.readUint32()
	}
	if t.Value.MetaSpell.SpType == 5 || t.Value.MetaSpell.SpType == 6 {
		t.Value.MetaSpell.Spell.Index = s.readInt32()
	}
	if t.Value.MetaSpell.SpType == 7 {
		t.Value.MetaSpell.Spell.PortalLifetime = s.readDouble()
		t.Value.MetaSpell.Spell.Link = s.readInt32()
	}
	if t.Value.MetaSpell.SpType == 8 || t.Value.MetaSpell.SpType == 13 {
		t.Value.MetaSpell.Spell.POS = new(Position).Read(s)
	}
	if t.Value.MetaSpell.SpType == 9 || t.Value.MetaSpell.SpType == 14 {
		t.Value.MetaSpell.Spell.MinPower = s.readInt32()
		t.Value.MetaSpell.Spell.MaxPower = s.readInt32()
		t.Value.MetaSpell.Spell.PowerVariance = s.readSingle()
		t.Value.MetaSpell.Spell.School = s.readInt32()
		t.Value.MetaSpell.Spell.Align = s.readInt32()
		t.Value.MetaSpell.Spell.Number = s.readInt32()
		t.Value.MetaSpell.Spell.NumberVariance = s.readSingle()
	}

	t.Value.Formula = make([]uint32, 8)
	for i := range t.Value.Formula {
		t.Value.Formula[i] = *s.readUint32()
	}

	t.Value.CasterEffect = *s.readUint32()
	t.Value.TargetEffect = *s.readUint32()
	t.Value.FizzleEffect = *s.readUint32()
	t.Value.RecoveryInterval = *s.readDouble()
	t.Value.RecoveryAmount = *s.readSingle()
	t.Value.DisplayOrder = *s.readUint32()
	t.Value.NonComponentTargetType = *s.readUint32()
	t.Value.ManaMod = *s.readUint32()

	return t
}

type StatMod struct {
	Key  uint32  `json:"key"`
	Type uint32  `json:"type"`
	Val  float32 `json:"val"`
}

func (t *StatMod) Read(s *SBuffer) *StatMod {
	binary.Read(s, binary.LittleEndian, t)
	return t
}
