package ac

import (
	"encoding/binary"
	"fmt"
	"time"
)

type DiskFileInfo_t struct {
	Magic               uint32      `json:"magic"`
	BlockSize           uint32      `json:"block_size"`
	FileSize            uint32      `json:"file_size"`
	DataSetLM           DatFileType `json:"data_set_lm"`
	DataSubsetLM        uint32      `json:"data_subset_lm"`
	FirstFree           uint32      `json:"first_free"`
	FinalFree           uint32      `json:"final_free"`
	FreeBlocks          uint32      `json:"free_blocks"`
	BTreeRoot           uint32      `json:"btree_root"`
	YoungLRULM          uint32      `json:"young_lru_lm"`
	OldLRULM            uint32      `json:"old_lru_lm"`
	UseLRUFM            uint32      `json:"use_lru_fm"`
	MasterMapID         uint32      `json:"master_map_id"`
	EngineVersionNumber uint32      `json:"engine_version"`
	GameVersionNumber   uint32      `json:"game_version"`
	MajorVersionNumber  GUID        `json:"guid"`
	MinorVersionNumber  uint32      `json:"version"`
}

type GUID struct {
	Data1 uint32
	Data2 uint16
	Data3 uint16
	Data4 [8]uint8
}

func (g *GUID) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%08x-%04x-%04x-%016x\"", g.Data1, g.Data2, g.Data3, binary.LittleEndian.Uint64(g.Data4[:]))), nil
}

type BTEntry struct {
	Bitmask uint32 `json:"bitmask"` /* unsigned __int32 comp_ : 1; unsigned __int32 resv_ : 15; unsigned __int32 ver_ : 16; */
	GID     uint32 `json:"gid"`
	Offset  uint32 `json:"offset"`
	Size    uint32 `json:"size"`
	Date    uint32 `json:"date"`
	Iter    uint32 `json:"iter"`
}

func (e *BTEntry) ToString(dat *DatFile) string {
	comp := e.Bitmask >> 31
	resv := (e.Bitmask >> 16) & 0x00007FFF
	ver := e.Bitmask & 0x0000FFFF
	return fmt.Sprintf("comp:%d,resv:%d,ver:%d GID: 0x%08X, Offset: 0x%08X, Size: %d, Date: %s, Iter: %d",
		comp, resv, ver, e.GID, e.Offset, e.Size, time.Unix(int64(e.Date), 0), e.Iter)
}

type BTNode struct {
	NextNode   [62]uint32
	NumEntries uint32
	Entries    [61]BTEntry
}
