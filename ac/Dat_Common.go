package ac

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
)

type DatFileType uint32

var DATFILE_TYPE []string = []string{"UNDEF_DISK", "PORTAL_DATFILE", "CELL_DATFILE", "LOCAL_DATFILE"}
var DatPortalType []string = []string{}

func (d DatFileType) ToString() string {
	var t string = "Unknown"
	if int(d) < len(DATFILE_TYPE) {
		t = DATFILE_TYPE[d]
	}
	return fmt.Sprintf("%s (%d)", t, d)
}

type Dat_LightInfo struct {
	ViewerSpaceLocation Frame   `json:"viewer_space_location"`
	Color               uint32  `json:"color"`
	Intensity           float32 `json:"intensity"`
	Falloff             float32 `json:"falloff"`
	ConeAngle           float32 `json:"cone_angle"`
}

func (t *Dat_LightInfo) Read(s *SBuffer) *Dat_LightInfo {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_CylSphere struct {
	Origin Vector3 `json:"origin"`
	Radius float32 `json:"radius"`
	Height float32 `json:"height"`
}

func (t *Dat_CylSphere) Read(s *SBuffer) *Dat_CylSphere {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Sphere struct {
	Origin Vector3 `json:"origin"`
	Radius float32 `json:"radius"`
}

func (t *Dat_Sphere) Read(s *SBuffer) *Dat_Sphere {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Stab struct {
	Id    uint32 `json:"id"`
	Frame Frame  `json:"frame"`
}

func (t *Dat_Stab) Read(s *SBuffer) *Dat_Stab {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_CellPortal struct {
	Flags         uint16 `json:"flags"`
	PolygonId     uint16 `json:"polygon_id"`
	OtherCellId   uint16 `json:"other_cell_id"`
	OtherPortalId uint16 `json:"other_portal_id"`
}

func (t *Dat_CellPortal) Read(s *SBuffer) *Dat_CellPortal {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_CBldPortal struct {
	Flags         uint16   `json:"flags"`
	OtherCellId   uint16   `json:"other_cell_id"`
	OtherPortalId uint16   `json:"other_portal_id"`
	NumStabs      uint16   `json:"num_stabs"`
	StabList      []uint16 `json:"stab_list"`
}

func (t *Dat_CBldPortal) Read(s *SBuffer) *Dat_CBldPortal {
	t.Flags = *s.readUint16()
	t.OtherCellId = *s.readUint16()
	t.OtherPortalId = *s.readUint16()
	t.NumStabs = *s.readUint16()
	t.StabList = *s.readUint16s(int(t.NumStabs))
	if t.NumStabs&1 == 1 { // very janky alignment.
		s.readUint16()
	}
	return t
}

type Dat_Iteration struct {
	I      []int32 `json:"i"`
	Sorted bool    `json:"sorted"`
}

func (t *Dat_Iteration) Read(s *SBuffer) *Dat_Iteration {
	t.I = *s.readInt32s(2)
	t.Sorted = *s.readByte() == 1
	return t
}

type Dat_HashEntry struct {
	Key   uint32 `json:"key"`
	Value uint32 `json:"value"`
}

func (t *Dat_HashEntry) Read(s *SBuffer) *Dat_HashEntry {
	t.Key = *s.readUint32()
	t.Value = *s.readUint32()
	return t
}

type Dat_Terrain uint16

// func (t Dat_Terrain) MarshalJSON() ([]byte, error) {
// 	//st := (t << 8) | (t >> 8)
// 	return []byte(fmt.Sprintf("{\"terrain\":%d,\"road\":%d,\"type\":%d,\"scenery\":%d}",
// 		t,
// 		t&3,
// 		(t&0x7C)>>2,
// 		(t >> 11),
// 	)), nil
// }

type Dat_LandBlock struct {
	Id         uint32          `json:"id"`
	HasObjects uint32          `json:"has_objects"`
	Terrain    [81]Dat_Terrain `json:"terrain"`
	Height     [81]uint8       `json:"height"`
}

func (t *Dat_LandBlock) Read(s *SBuffer) *Dat_LandBlock {
	binary.Read(s, binary.LittleEndian, t)
	return t
}

type Dat_Attribute uint32
type Dat_Attribute2nd uint32

var Dat_Attributes []string = []string{"Undef", "Strength", "Endurance", "Quickness", "Coordination", "Focus", "Self"}
var Dat_Attributes2nd []string = []string{"Undef", "MaxHealth", "Health", "MaxStamina", "Stamina", "MaxMana", "Mana"}

func (t Dat_Attribute) MarshalJSON() ([]byte, error) {
	if int(t) >= len(Dat_Attributes) {
		t = 0
	}
	return []byte(fmt.Sprintf("{\"id\":%d,\"name\":\"%s\"}", t, Dat_Attributes[t])), nil
}
func (t Dat_Attribute2nd) MarshalJSON() ([]byte, error) {
	if int(t) >= len(Dat_Attributes2nd) {
		t = 0
	}
	return []byte(fmt.Sprintf("{\"id\":%d,\"name\":\"%s\"}", t, Dat_Attributes2nd[t])), nil
}

type Dat_BuildInfo struct {
	ModelId    uint32           `json:"model_id"`
	Frame      Frame            `json:"frame"`
	NumLeaves  uint32           `json:"num_leaves"`
	NumPortals uint32           `json:"num_portals"`
	Portals    []Dat_CBldPortal `json:"portals"`
}

func (t *Dat_BuildInfo) Read(s *SBuffer) *Dat_BuildInfo {
	t.ModelId = *s.readUint32()
	t.Frame = *new(Frame).Read(s)
	t.NumLeaves = *s.readUint32()
	t.NumPortals = *s.readUint32()
	t.Portals = make([]Dat_CBldPortal, int(t.NumPortals))
	for i := range t.Portals {
		t.Portals[i].Read(s)
	}
	return t
}

type Dat_PlacementType struct {
	AnimationFrame []Dat_AnimationFrame `json:"animation_frame"`
}

func (t *Dat_PlacementType) Read(s *SBuffer, num_parts int) *Dat_PlacementType {
	t.AnimationFrame = make([]Dat_AnimationFrame, num_parts)
	for i := range t.AnimationFrame {
		t.AnimationFrame[i].Read(s, num_parts)
	}
	return t
}

type Dat_LocationType struct {
	PartId uint32 `json:"id"`
	Frame  Frame  `json:"frame"`
}

func (t *Dat_LocationType) Read(s *SBuffer) *Dat_LocationType {
	t.PartId = *s.readUint32()
	t.Frame = *new(Frame).Read(s)
	return t
}

type Dat_PString string

func (t *Dat_PString) Read(s *SBuffer, un_obfuscate bool) *Dat_PString {
	var length int = int(*s.readUint16())
	res := s.Next(length)
	if un_obfuscate {
		for i := range res {
			res[i] = (res[i] >> 4) | (res[i] << 4)
		}
	}
	*t = Dat_PString(res)
	length += 2
	for length%4 != 0 {
		s.readByte()
		length++
	}
	return t
}

type Dat_CompressedUInt32 uint32

func (t *Dat_CompressedUInt32) Read(s *SBuffer) *Dat_CompressedUInt32 {
	var ret uint32 = uint32(*s.readByte())
	if (ret & 0x80) == 0 {
		*t = Dat_CompressedUInt32(ret)
		return t
	}
	if (ret & 0x40) == 0 {
		ret |= (uint32(*s.readByte()) << 8) & 0x7FFF
		*t = Dat_CompressedUInt32(ret)
		return t
	}
	ret |= (uint32(*s.readUint16()) << 16) & 0x3FFFFFFF
	*t = Dat_CompressedUInt32(ret)
	return t
}

type Dat_CompressedDID uint32

func (t *Dat_CompressedDID) Read(s *SBuffer, did_class uint32) *Dat_CompressedDID {
	val := *s.readUint16()
	if (val & 0x8000) == 0 {
		*t = Dat_CompressedDID(uint32(val) + did_class)
		return t
	}
	lower := *s.readUint16()
	*t = Dat_CompressedDID((uint32(val&0x3FFF) << 16) + uint32(lower) + did_class)
	return t
}

func ToJSON(in any) string {
	resu, _ := json.Marshal(in)
	return string(resu)
}
func ToJSONFormat(in any, prefix, indent string) string {
	resu, _ := json.MarshalIndent(in, prefix, indent)
	return string(resu)
}

type Dat_AttackCone struct {
	PartIndex uint32  `json:"hook_type"`
	LeftX     float32 `json:"left_x"`
	LeftY     float32 `json:"left_y"`
	RightX    float32 `json:"right_x"`
	RightY    float32 `json:"right_y"`
	Radius    float32 `json:"radius"`
	Height    float32 `json:"height"`
}

func (t *Dat_AttackCone) Read(s *SBuffer) *Dat_AttackCone {
	binary.Read(s, binary.LittleEndian, t)
	return t
}
