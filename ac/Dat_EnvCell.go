package ac

import (
	"fmt"
)

type Dat_EnvCell struct {
	Id             uint32            `json:"id"`                 // offset 0
	Flags          uint32            `json:"flags"`              // offset 4
	CellID         uint32            `json:"cell_id"`            // offset 8
	NumSurfaces    uint8             `json:"num_surfaces"`       // offset 12
	NumPortals     uint8             `json:"num_portals"`        // offset 13
	NumStabs       uint16            `json:"num_stabs"`          // offset 14
	Surfaces       []Dat_Surface     `json:"surfaces,omitempty"` // len: NumSurfaces, Add 0x08000000 for DID
	EnvironmentId  Dat_EnvironmentId `json:"environment_id"`     // Add 0x0D000000 for DID
	CellStructure  uint16            `json:"cell_structure"`
	Position       Frame             `json:"position"`
	Portals        []Dat_CellPortal  `json:"portals,omitempty"`         // len: NumPortals
	VisibleCells   []uint16          `json:"visible_cells,omitempty"`   // len: NumStabs
	StaticObjects  []Dat_Stab        `json:"static_objects,omitempty"`  // len: (Flags & 2)>>1
	RestrictionObj []uint32          `json:"restriction_obj,omitempty"` // len: (Flags & 8)>>3
}

func (t *Dat_EnvCell) Read(s *SBuffer) *Dat_EnvCell {
	t.Id = *s.readUint32()
	t.Flags = *s.readUint32()
	t.CellID = *s.readUint32()
	t.NumSurfaces = *s.readByte()
	t.NumPortals = *s.readByte()
	t.NumStabs = *s.readUint16()
	t.Surfaces = make([]Dat_Surface, int(t.NumSurfaces))
	for i := range t.Surfaces {
		t.Surfaces[i] = Dat_Surface(*s.readUint16())
	}
	t.EnvironmentId = Dat_EnvironmentId(*s.readUint16())
	t.CellStructure = *s.readUint16()
	t.Position = *new(Frame).Read(s)
	t.Portals = make([]Dat_CellPortal, int(t.NumPortals))
	for i := range t.Portals {
		t.Portals[i].Read(s)
	}
	t.VisibleCells = make([]uint16, int(t.NumStabs))
	for i := range t.VisibleCells {
		t.VisibleCells[i] = *s.readUint16()
	}
	if (t.Flags & 2) == 2 {
		t.StaticObjects = make([]Dat_Stab, 1)
		t.StaticObjects[0].Read(s)
	}
	if (t.Flags & 8) == 8 {
		t.RestrictionObj = *s.readUint32s(1)
	}
	return t
}

type Dat_Surface uint16

type Dat_EnvironmentId uint16

func (t Dat_Surface) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%d", uint32(t)+0x08000000)), nil
}
func (t Dat_EnvironmentId) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%d", uint32(t)+0x0D000000)), nil
}
