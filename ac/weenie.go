package ac

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
)

var Weenie_m sync.Mutex
var Weenies = make(map[uint32]*Weenie)
var Weenie_IDs = make([]uint32, 0)
var Weenie_Name = make(map[string]*Weenie)
var Weenie_Names = make([]string, 0)
var Weenie_Type = make(map[uint32]*[]*Weenie)
var Weenie_Types = make([]uint32, 0)

func Weenie_Stats() {
	log.Printf("Total Weenies: %d == %d, %d thru %d\n", len(Weenies), len(Weenie_IDs), Weenie_IDs[0], Weenie_IDs[len(Weenie_IDs)-1])
	log.Printf("Total Weenie_Names: %d == %d, \"%s\" thru \"%s\"\n", len(Weenie_Name), len(Weenie_Names), Weenie_Names[0], Weenie_Names[len(Weenie_Names)-1])
	log.Printf("Total Weenie_Types: %d == %d, %d thru %d\n", len(Weenie_Type), len(Weenie_Types), Weenie_Types[0], Weenie_Types[len(Weenie_Types)-1])
}
func Weenie_Parse(root string) error {
	Weenie_m.Lock()
	defer Weenie_m.Unlock()
	Weenies = make(map[uint32]*Weenie)
	Weenie_IDs = make([]uint32, 0)
	Weenie_Name = make(map[string]*Weenie)
	Weenie_Names = make([]string, 0)
	Weenie_Type = make(map[uint32]*[]*Weenie)
	Weenie_Types = make([]uint32, 0)
	recurseParseWeenie(filepath.Join(root, "cache\\weenies"))
	recurseParseWeenie(filepath.Join(root, "weenies"))
	sort.SliceStable(Weenie_IDs, func(i, j int) bool {
		return Weenie_IDs[i] < Weenie_IDs[j]
	})
	sort.SliceStable(Weenie_Types, func(i, j int) bool {
		return Weenie_Types[i] < Weenie_Types[j]
	})
	sort.Strings(Weenie_Names)
	return nil
}

func recurseParseWeenie(path string) {
	edir, err := os.ReadDir(path)
	if err != nil {
		log.Printf("Could not open \"%s\" folder: %v", path, err)
		return
	}
	for _, e := range edir {
		info, _ := e.Info()
		file := path + "/" + e.Name()
		if info.IsDir() {
			recurseParseWeenie(file)
			continue
		}
		if info.Size() == 0 {
			continue
		}
		if !strings.HasSuffix(file, ".json") {
			continue
		}
		content, err := os.ReadFile(file)
		if err != nil {
			log.Printf("ERROR: Unable to read Weenie %s: %s", file, err)
			continue
		}
		var w Weenie
		if err := json.Unmarshal(content, &w); err != nil {
			log.Printf("ERROR: failed to parse %s: %v", file, err)
			continue
		}
		w.Source = file
		if previous, ok := Weenies[w.WCID]; ok {
			if !strings.Contains(previous.Source, "\\cache\\") {
				log.Printf("ERROR: duplicate WCID %d %s", w.WCID, file)
				continue
			} else {
				Weenies[w.WCID] = &w
			}
		} else {
			Weenies[w.WCID] = &w
			Weenie_IDs = append(Weenie_IDs, w.WCID)
		}
		type_arr, ok := Weenie_Type[w.WeenieType]
		if !ok {
			Weenie_Type[w.WeenieType] = &[]*Weenie{&w}
			Weenie_Types = append(Weenie_Types, w.WeenieType)
		} else {
			*type_arr = append(*type_arr, &w)
		}
		Weenie_name := w.GetName()

		_, ok = Weenie_Name[Weenie_name]
		if !ok {
			Weenie_Name[Weenie_name] = &w
			Weenie_Names = append(Weenie_Names, Weenie_name)
		}
		wcidString := fmt.Sprintf("%d", w.WCID)
		if !strings.HasPrefix(filepath.Base(file), wcidString) {
			log.Printf("Warning: %s should be named %s - %s.json\n", filepath.Base(file), wcidString, Weenie_name)
		}
	}
}

func (W *Weenie) GetName() string {
	Weenie_name := "Error"
	if W.StringStats != nil {
		for _, f := range W.StringStats {
			if f.Key == 1 {
				Weenie_name = string(f.Value)
				break
			}
		}
	}
	return Weenie_name
}

func Weenie_Dump(wcid uint32) {

	if weenie, ok := Weenies[wcid]; ok {
		fmt.Printf("\n\nSource: %s\n", weenie.Source)
		fmt.Printf("WCID: %d (%s)\n", weenie.WCID, weenie.GetName())

		var weenie_type string = "Unknown"
		if weenie.WeenieType < uint32(len(WeenieType)) {
			weenie_type = WeenieType[weenie.WeenieType]
		}

		fmt.Printf("WeenieType: %s (%d)\n", weenie_type, weenie.WeenieType)

		if weenie.Attributes != nil {
			fmt.Printf("STR: %d, END: %d, COORD: %d, QUICK: %d, FOCUS: %d, SELF: %d\n", weenie.Attributes.Strength.InitLevel, weenie.Attributes.Endurance.InitLevel, weenie.Attributes.Coordination.InitLevel, weenie.Attributes.Quickness.InitLevel, weenie.Attributes.Focus.InitLevel, weenie.Attributes.Self.InitLevel)
			fmt.Printf("HP: %d, STAM: %d, MANA: %d\n", weenie.Attributes.Health.InitLevel+(weenie.Attributes.Endurance.InitLevel/2), weenie.Attributes.Stamina.InitLevel+weenie.Attributes.Endurance.InitLevel, weenie.Attributes.Mana.InitLevel+weenie.Attributes.Self.InitLevel)
		}
		if weenie.Body != nil && len(weenie.Body.BodyPartTable) > 0 {
			fmt.Printf("Body: %d\n", len(weenie.Body.BodyPartTable))
			for _, bodypart := range weenie.Body.BodyPartTable {
				fmt.Printf("\tKey: %d\tBh: %s  Dtype: %s Dval: %d Dvar: %f\n\t\tArmorCache: %s\n\t\tBpsd: %s\n",
					bodypart.Key, bodypart.Value.Bh.ToString(), bodypart.Value.Dtype.ToString(), bodypart.Value.Dval, bodypart.Value.Dvar,
					bodypart.Value.Acache.ToString(),
					bodypart.Value.Bpsd.ToString(),
				)
			}
		}
		if weenie.BoolStats != nil {
			fmt.Printf("BoolStats: %d\n", len(weenie.BoolStats))
			for _, stat := range weenie.BoolStats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.IntStats != nil && len(weenie.IntStats) > 0 {
			fmt.Printf("IntStats: %d: %s\n", len(weenie.IntStats), ToJSON(weenie.IntStats))
		}
		if weenie.DIDStats != nil && len(weenie.DIDStats) > 0 {
			fmt.Printf("DIDStats: %d\n", len(weenie.DIDStats))
			for _, stat := range weenie.DIDStats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.IIDStats != nil && len(weenie.IIDStats) > 0 {
			fmt.Printf("IIDStats: %d\n", len(weenie.IIDStats))
			for _, stat := range weenie.IIDStats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.FloatStats != nil && len(weenie.FloatStats) > 0 {
			fmt.Printf("FloatStats: %d\n", len(weenie.FloatStats))
			for _, stat := range weenie.FloatStats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.Int64Stats != nil && len(weenie.Int64Stats) > 0 {
			fmt.Printf("Int64Stats: %d\n", len(weenie.Int64Stats))
			for _, stat := range weenie.Int64Stats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.StringStats != nil && len(weenie.StringStats) > 0 {
			fmt.Printf("StringStats: %d\n", len(weenie.StringStats))
			for _, stat := range weenie.StringStats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.PositionStats != nil && len(weenie.PositionStats) > 0 {
			fmt.Printf("PositionStats: %d\n", len(weenie.PositionStats))
			for _, stat := range weenie.PositionStats {
				fmt.Printf("\t%s\n", stat.ToString())
			}
		}
		if weenie.Skills != nil && len(weenie.Skills) > 0 {
			fmt.Printf("Skills: %d\n", len(weenie.Skills))
			for _, skill := range weenie.Skills {
				var skill_name string = "Unknown"
				if skill.Key < uint32(len(STypeSkill)) {
					skill_name = STypeSkill[skill.Key]
				}
				fmt.Printf("\t%s (%d): %#v\n", skill_name, skill.Key, skill.Value)
			}
		}
		if weenie.SpellBook != nil && len(weenie.SpellBook) > 0 {
			fmt.Printf("Spellbook: %d\n", len(weenie.SpellBook))
			for _, spell := range weenie.SpellBook {
				fmt.Printf("\t%s\n", spell.ToString())
			}
		}
		if weenie.CreateList != nil && len(weenie.CreateList) > 0 {
			fmt.Printf("CreateList: %d\n", len(weenie.CreateList))
			for _, c := range weenie.CreateList {
				fmt.Printf("\t%s\n", c.ToString())
			}
		}
		if weenie.EmoteTable != nil && len(weenie.EmoteTable) > 0 {
			fmt.Printf("EmoteTables: %d\n", len(weenie.EmoteTable))
			for _, e := range weenie.EmoteTable {
				if len(e.Value) > 0 {
					var reta []string = []string{}

					for _, em := range e.Value {
						reta = append(reta, fmt.Sprintf("\t\t%s\n", em.ToString()))
					}

					fmt.Printf("\tCategory %s (%d)\n%s", e.Key.ToString(), int(e.Key), strings.Join(reta, ""))
				}
			}
		}
		if weenie.GeneratorTable != nil && len(weenie.GeneratorTable) > 0 {
			fmt.Printf("GeneratorTable: %#v\n", weenie.GeneratorTable)
		}
		if weenie.general != nil {
			fmt.Printf("general: %#v\n", *weenie.general)
		}

		return
	} else {
		fmt.Printf("WCID is invalid %d", wcid)
	}
}

type Weenie struct {
	Source string `json:"-"`
	WCID   uint32 `json:"wcid"`
	CACQualities
	*general
}
