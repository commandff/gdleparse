package ac

import (
	"bytes"
	"fmt"
	"image"
	"log"
	"os"

	"github.com/sunshineplan/imgconv"
)

type Dat_Texture struct {
	Id      uint32 `json:"id"`
	Unknown uint32 `json:"unknown"` // unknownVals:map[uint32]int{0x0:9, 0x1:206, 0x2:86, 0x3:42, 0x4:2049, 0x5:1042, 0x6:13253, 0x7:2243, 0x8:1364, 0x9:304, 0xa:86}
	// unknownVals:map[uint32]int{0x1:2, 0x2:8, 0x4:46, 0x5:476, 0x7:762, 0x8:998, 0x9:2},
	Width            uint32  `json:"width"`
	Height           uint32  `json:"height"`
	Format           uint32  `json:"format"`
	Length           uint32  `json:"length"`
	Data             []byte  `json:"-"`
	DefaultPaletteId *uint32 `json:"default_palette_id,omitempty"`
}

func (t *Dat_Texture) ToPNG() (string, []byte) {
	var src image.Image

	var in bytes.Buffer
	var pos int = 0
	switch t.Format {

	case 20: // PFID_R8G8B8
		for y := int(0); y < int(t.Height); y++ {
			for x := int(0); x < int(t.Width); x++ {
				in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos], 0xFF})
				pos += 3
			}
		}
		src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	case 243: // PFID_CUSTOM_LSCAPE_R8G8B8
		for y := int(0); y < int(t.Height); y++ {
			for x := int(0); x < int(t.Width); x++ {
				in.Write([]byte{t.Data[pos], t.Data[pos+1], t.Data[pos+2], 0xFF})
				pos += 3
			}
		}
		src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	case 21: // PFID_A8R8G8B8
		for y := int(0); y < int(t.Height); y++ {
			for x := int(0); x < int(t.Width); x++ {
				in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
				pos += 4
			}
		}
		src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 23: // PFID_R5G6B5 = 23, // 0x17:3, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+1] << 3, (t.Data[pos] << 5) + (t.Data[pos+1] & 0x03), t.Data[pos] & 0xF8, 0xFF})
	// 			pos += 2
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 26: // PFID_A4R4G4B4 = 26, // 0x1a:2, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+1] & 0xF0, t.Data[pos] << 4, t.Data[pos] & 0xF0, t.Data[pos+1] << 4})
	// 			pos += 2
	// 		}
	// 	}

	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 28: // PFID_A8 = 28, // 0x1c:86, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 41: // PFID_P8 = 41, // 0x29:6, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 101: // PFID_INDEX16 = 101, // 0x65:4182, 1283,
	// 	in.Write(t.Data)
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 2, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 244: // PFID_CUSTOM_LSCAPE_ALPHA = 244, // 0xf4:16, 8,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 500: // PFID_CUSTOM_RAW_JPEG = 500, // 0x1f4:79, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 827611204: // PFID_DXT1 = 827611204, // 0x31545844:1971, 986,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 861165636: // PFID_DXT3 = 861165636, // 0x33545844:5, 0,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}
	// case 894720068: // PFID_DXT5 = 894720068, // 0x35545844:127, 1,
	// 	for y := int(0); y < int(t.Height); y++ {
	// 		for x := int(0); x < int(t.Width); x++ {
	// 			in.Write([]byte{t.Data[pos+2], t.Data[pos+1], t.Data[pos+0], t.Data[pos+3]})
	// 			pos += 4
	// 		}
	// 	}
	// 	src = &image.RGBA{Pix: in.Bytes(), Stride: int(t.Width) * 4, Rect: image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: int(t.Width), Y: int(t.Height)}}}

	default:
		return ToJSON(t), []byte{}
	}

	var out bytes.Buffer

	// Write the resulting image.
	err := imgconv.Write(&out, src, &imgconv.FormatOption{Format: imgconv.PNG})
	if err != nil {
		return ToJSON(t), []byte{} // log.Fatalf("failed to convert image: %v", err)
	}
	fileName := fmt.Sprintf("img/0x%08X.png", t.Id)
	f, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		log.Fatalf("failed to open %s: %v", fileName, err)
	}
	defer f.Close()

	_, err = f.Write(out.Bytes())
	if err != nil {
		log.Fatalf("failed to write %s: %v", fileName, err)
	}
	return ToJSON(t), out.Bytes()
}
func (t *Dat_Texture) Read(s *SBuffer) *Dat_Texture {
	t.Id = *s.readUint32()
	t.Unknown = *s.readUint32()
	t.Width = *s.readUint32()
	t.Height = *s.readUint32()
	t.Format = *s.readUint32()
	t.Length = *s.readUint32()
	t.Data = s.Next(int(t.Length))
	if t.Format == 101 || t.Format == 41 {
		t.DefaultPaletteId = s.readUint32()
	}
	return t
}
