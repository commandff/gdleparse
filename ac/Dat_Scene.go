package ac

import (
	"encoding/binary"
)

type Dat_Scene struct {
	Id           uint32           `json:"id"`
	TotalObjects uint32           `json:"total_objects"`
	Objects      []Dat_ObjectDesc `json:"objects"`
}

func (t *Dat_Scene) Read(s *SBuffer) *Dat_Scene {
	t.Id = *s.readUint32()
	t.TotalObjects = *s.readUint32()
	t.Objects = make([]Dat_ObjectDesc, int(t.TotalObjects))
	for i := range t.Objects {
		t.Objects[i].Read(s)
	}
	return t
}

type Dat_ObjectDesc struct {
	ObjId       uint32  `json:"obj_id"`
	BaseLoc     Frame   `json:"base_loc"`
	Freq        float32 `json:"freq"`
	DisplaceX   float32 `json:"displace_x"`
	DisplaceY   float32 `json:"displace_y"`
	MinScale    float32 `json:"min_scale"`
	MaxScale    float32 `json:"max_scale"`
	MaxRotation float32 `json:"max_rotation"`
	MinSlope    float32 `json:"min_slope"`
	MaxSlope    float32 `json:"max_slope"`
	Align       uint32  `json:"align"`
	Orient      uint32  `json:"orient"`
	WeenieObj   uint32  `json:"weenie_obj"`
}

func (t *Dat_ObjectDesc) Read(s *SBuffer) *Dat_ObjectDesc {
	binary.Read(s, binary.LittleEndian, t)
	return t
}
