package ac

type Dat_Animation struct {
	Id         uint32               `json:"id"`
	Flags      uint32               `json:"flags"`
	NumParts   uint32               `json:"num_parts"`
	NumFrames  uint32               `json:"num_frames"`
	PosFrames  []Frame              `json:"pos_frames"`
	PartFrames []Dat_AnimationFrame `json:"part_frames"`
}

func (t *Dat_Animation) Read(s *SBuffer) *Dat_Animation {
	t.Id = *s.readUint32()
	t.Flags = *s.readUint32()
	t.NumParts = *s.readUint32()
	t.NumFrames = *s.readUint32()
	if (t.Flags & 1) == 1 {
		t.PosFrames = make([]Frame, int(t.NumFrames))
		for i := range t.PosFrames {
			t.PosFrames[i] = *new(Frame).Read(s)
		}
	}
	t.PartFrames = make([]Dat_AnimationFrame, int(t.NumParts))
	for i := range t.PartFrames {
		t.PartFrames[i] = *new(Dat_AnimationFrame).Read(s, int(t.NumParts))
	}
	return t
}

type Dat_AnimationFrame struct {
	Frames   []Frame             `json:"frames"`
	NumHooks uint32              `json:"num_hooks"`
	Hooks    []Dat_AnimationHook `json:"hooks"`
}

func (t *Dat_AnimationFrame) Read(s *SBuffer, num_frames int) *Dat_AnimationFrame {
	t.Frames = make([]Frame, num_frames)
	for i := range t.Frames {
		t.Frames[i] = *new(Frame).Read(s)
	}
	t.NumHooks = *s.readUint32()
	t.Hooks = make([]Dat_AnimationHook, int(t.NumHooks))
	for i := range t.Hooks {
		t.Hooks[i].Read(s)
	}

	return t
}

type Dat_AnimationHook struct {
	HookType      uint32          `json:"HookType"`
	Direction     int32           `json:"Direction"`
	Id            *uint32         `json:"Id,omitempty"`
	SoundType     *uint32         `json:"SoundType,omitempty"`
	AttackCone    *Dat_AttackCone `json:"AttackCone,omitempty"`
	APChange      *AnimPartChange `json:"APChange,omitempty"`
	Ethereal      *int32          `json:"Ethereal,omitempty"`
	Part          *uint32         `json:"Part,omitempty"`
	Start         *float32        `json:"Start,omitempty"`
	End           *float32        `json:"End,omitempty"`
	Time          *float32        `json:"Time,omitempty"`
	EmitterId     *uint32         `json:"EmitterId,omitempty"`
	NoDraw        *uint32         `json:"NoDraw,omitempty"`
	PartIndex     *uint32         `json:"PartIndex,omitempty"`
	PES           *uint32         `json:"PES,omitempty"`
	Pause         *float32        `json:"Pause,omitempty"`
	SoundID       *uint32         `json:"SoundID,omitempty"`
	Priority      *float32        `json:"Priority,omitempty"`
	Probability   *float32        `json:"Probability,omitempty"`
	Volume        *float32        `json:"Volume,omitempty"`
	Axis          *Vector3        `json:"Axis,omitempty"`
	USpeed        *float32        `json:"USpeed,omitempty"`
	VSpeed        *float32        `json:"VSpeed,omitempty"`
	LightsOn      *int32          `json:"LightsOn,omitempty"`
	EmitterInfoId *uint32         `json:"EmitterInfoId,omitempty"`
	Offset        *Frame          `json:"Offset,omitempty"`
}

func (t *Dat_AnimationHook) Read(s *SBuffer) *Dat_AnimationHook {
	t.HookType = *s.readUint32()
	t.Direction = *s.readInt32()
	switch t.HookType {
	case 1: // Sound
		t.Id = s.readUint32()
	case 2: // SoundTable
		t.SoundType = s.readUint32()
	case 3: // Attack
		t.AttackCone = new(Dat_AttackCone).Read(s)
	case 4: // AnimationDone:
	// NO BODY
	case 5: // ReplaceObject
		t.APChange = new(AnimPartChange).Read(s)
	case 6: // Ethereal
		t.Ethereal = s.readInt32()
	case 7: // TransparentPart:
		t.Part = s.readUint32()
		t.Start = s.readSingle()
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 8: // Luminous:
		t.Start = s.readSingle()
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 9: // LuminousPart:
		t.Part = s.readUint32()
		t.Start = s.readSingle()
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 10: // Diffuse:
		t.Start = s.readSingle()
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 11: // DiffusePart:
		t.Part = s.readUint32()
		t.Start = s.readSingle()
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 12: // Scale:
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 13: // CreateParticle:
		t.EmitterInfoId = s.readUint32()
		t.PartIndex = s.readUint32()
		t.Offset = new(Frame).Read(s)
		t.EmitterId = s.readUint32()
	case 14: // DestroyParticle:
		t.EmitterId = s.readUint32()
	case 15: // StopParticle:
		t.EmitterId = s.readUint32()
	case 16: // NoDraw:
		t.NoDraw = s.readUint32()
	case 17: // DefaultScript:
		// NO BODY
	case 18: // DefaultScriptPart:
		t.PartIndex = s.readUint32()
	case 19: // CallPES:
		t.PES = s.readUint32()
		t.Pause = s.readSingle()
	case 20: // Transparent:
		t.Start = s.readSingle()
		t.End = s.readSingle()
		t.Time = s.readSingle()
	case 21: // SoundTweaked:
		t.SoundID = s.readUint32()
		t.Priority = s.readSingle()
		t.Probability = s.readSingle()
		t.Volume = s.readSingle()
	case 22: // SetOmega:
		t.Axis = new(Vector3).Read(s)
	case 23: // TextureVelocity:
		t.USpeed = s.readSingle()
		t.VSpeed = s.readSingle()
	case 24: // TextureVelocityPart:
		t.PartIndex = s.readUint32()
		t.USpeed = s.readSingle()
		t.VSpeed = s.readSingle()
	case 25: // SetLight:
		t.LightsOn = s.readInt32()
	case 26: // CreateBlockingParticle:
		// NO BODY
	}

	return t
}

type AnimPartChange struct {
	PartIndex byte              `json:"part_index"`
	PartID    Dat_CompressedDID `json:"part_id"`
}

func (t *AnimPartChange) Read(s *SBuffer) *AnimPartChange {
	t.PartIndex = *s.readByte()
	t.PartID = *new(Dat_CompressedDID).Read(s, 0x01000000)
	return t
}

type TextureMapChange struct {
	PartIndex byte              `json:"part_index"`
	OldTexID  Dat_CompressedDID `json:"old_tex_id"`
	NewTexID  Dat_CompressedDID `json:"new_tex_id"`
}

func (t *TextureMapChange) Read(s *SBuffer) *TextureMapChange {
	t.PartIndex = *s.readByte()
	t.OldTexID = *new(Dat_CompressedDID).Read(s, 0x05000000)
	t.NewTexID = *new(Dat_CompressedDID).Read(s, 0x05000000)
	return t
}

type Subpalette struct {
	SubID     Dat_CompressedDID `json:"subID"`
	Offset    uint32            `json:"offset"`
	NumColors uint32            `json:"numcolors"`
}

func (t *Subpalette) Read(s *SBuffer) *Subpalette {
	t.SubID = *new(Dat_CompressedDID).Read(s, 0x04000000)
	t.Offset = uint32(*s.readByte()) << 3
	t.NumColors = (uint32(*s.readByte()) | 0x0100) << 3
	return t
}
