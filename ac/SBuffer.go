package ac

import (
	"bytes"
	"encoding/binary"
	"math"
)

type SBuffer struct{ *bytes.Buffer }

func (s *SBuffer) readByte() *byte {
	ret, _ := s.ReadByte()
	// var ret byte
	// binary.Read(s, binary.LittleEndian, &ret)
	return &ret
}

func (s *SBuffer) readUint16() *uint16 {
	ret := binary.LittleEndian.Uint16(s.Next(2))
	return &ret
}
func (s *SBuffer) readUint16s(size int) *[]uint16 {
	ret := make([]uint16, size)
	binary.Read(s, binary.LittleEndian, &ret)
	return &ret
}

func (s *SBuffer) readInt16() *int16 {
	ret := int16(binary.LittleEndian.Uint16(s.Next(2)))
	return &ret
}

func (s *SBuffer) readInt16s(size int) *[]int16 {
	ret := make([]int16, size)
	binary.Read(s, binary.LittleEndian, &ret)
	return &ret
}

func (s *SBuffer) readUint32() *uint32 {
	ret := binary.LittleEndian.Uint32(s.Next(4))
	return &ret
}
func (s *SBuffer) readUint32s(size int) *[]uint32 {
	ret := make([]uint32, size)
	binary.Read(s, binary.LittleEndian, &ret)
	return &ret
}

func (s *SBuffer) readInt32() *int32 {
	ret := int32(binary.LittleEndian.Uint32(s.Next(4)))
	return &ret
}
func (s *SBuffer) readInt32s(size int) *[]int32 {
	ret := make([]int32, size)
	binary.Read(s, binary.LittleEndian, &ret)
	return &ret
}

func (s *SBuffer) readUint64() *uint64 {
	ret := binary.LittleEndian.Uint64(s.Next(8))
	return &ret
}

func (s *SBuffer) readInt64() *int64 {
	ret := int64(binary.LittleEndian.Uint64(s.Next(8)))
	return &ret
}

func (s *SBuffer) readSingle() *float32 {
	ret := math.Float32frombits(binary.LittleEndian.Uint32(s.Next(4)))
	return &ret
}
func (s *SBuffer) readDouble() *float64 {
	ret := math.Float64frombits(binary.LittleEndian.Uint64(s.Next(8)))
	return &ret
}
