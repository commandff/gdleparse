package ac

type Dat_LandBlockInfo struct {
	Id                uint32          `json:"id"`
	NumCells          uint32          `json:"num_cells"`
	NumObjects        uint32          `json:"num_objects"`
	Objects           []Dat_Stab      `json:"objects"`
	NumBuildings      uint16          `json:"num_buildings"`
	PackMask          uint16          `json:"pack_mask"`
	Buildings         []Dat_BuildInfo `json:"buildings"`
	TotalObjects      uint16          `json:"total_objects"`
	BucketSize        uint16          `json:"bucket_size"`
	RestrictionTables []Dat_HashEntry `json:"restriction_tables"`
}

func (t *Dat_LandBlockInfo) Read(s *SBuffer) *Dat_LandBlockInfo {
	t.Id = *s.readUint32()
	t.NumCells = *s.readUint32()
	t.NumObjects = *s.readUint32()
	t.Objects = make([]Dat_Stab, int(t.NumObjects))
	for i := range t.Objects {
		t.Objects[i].Read(s)
	}
	t.NumBuildings = *s.readUint16()
	t.PackMask = *s.readUint16()
	t.Buildings = make([]Dat_BuildInfo, int(t.NumBuildings))
	for i := range t.Buildings {
		t.Buildings[i].Read(s)
	}
	if (t.PackMask & 1) == 1 {
		t.RestrictionTables = []Dat_HashEntry{{}}
		t.RestrictionTables[0].Read(s)
	}
	return t
}
